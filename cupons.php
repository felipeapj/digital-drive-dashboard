<?php 
    session_start();
    include_once 'connect.php';
    if(!isset ($_SESSION['logado']))
    	header('location:index.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Digital Drive</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link rel = "stylesheet" href = "https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="css/materialize.css">
        <link rel="stylesheet" href="css/style.css">
        <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <meta charset="utf8">
    </head>
    
    <script type="text/javascript" language="javascript">
        var mdLoading;
        $(document).ready(function(){
            $('.fixed-action-btn').floatingActionButton();
            $('.modal').modal();
            $('.sidenav').sidenav({
                edge: 'left', // Choose the horizontal origin
                closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
                draggable: true
            });            
            $('.collapsible').collapsible();
            mdLoading = M.Modal.getInstance(document.getElementById('modal_loading'));
            $('select').formSelect();
        });

        function modalEdit(op, id, nome, codigo, desconto, tipo){
            if (op=="delete"){
                document.getElementById('delid').value = id;
                document.getElementById('delnome').value = nome;
                document.getElementById('delcodigo').value = codigo;
            }
            else {
                document.getElementById('edtid').value = id;
                document.getElementById('edtnome').value = nome;
                document.getElementById('edtcodigo').value = codigo;
                document.getElementById('edtdesconto').value = desconto;
                document.getElementById('edttipo').selectedIndex = (tipo == 'PORCENTAGEM' ? 0 : 1);
                $('select').formSelect();
            }
            M.updateTextFields();
        }
        
        function enviar(op){            
            var http = new XMLHttpRequest();
            var url = 'cuponscontroller.php';
            mdLoading.open({opacity:1});
            http.open('POST', url, true);
            http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            switch (op){
                case 'delete':
                    var id = document.getElementById('delid').value;
                    var params = "op=delete&id="+id;                    
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Cupom deletado', displayLength : 2000});
                                document.getElementById("tb_cupons").innerHTML = location.reload();
                            }
                            else
                                M.toast({html: 'Erro ao deletar cupom', displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
                case 'update':
                    var id = document.getElementById('edtid').value;
                    var nome = document.getElementById('edtnome').value;
                    var codigo = document.getElementById('edtcodigo').value;
                    var desconto = document.getElementById('edtdesconto').value;
                    var tipo = document.getElementById('edttipo').value;
                    var params = "op=update&id="+id+"&nome="+nome+"&codigo="+codigo+"&desconto="+desconto+"&tipo="+tipo;
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Cupom alterado', displayLength : 2000});
                                document.getElementById("tb_cupons").innerHTML = location.reload();
                            }
                            else
                                M.toast({html: 'Erro ao alterar cupom', displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
                case 'insert':
                    var nome = document.getElementById('addnome').value;
                    var codigo = document.getElementById('addcodigo').value;
                    var desconto = document.getElementById('adddesconto').value;
                    var tipo = document.getElementById('addtipo').value;
                    var params = "op=insert&nome="+nome+"&codigo="+codigo+"&desconto="+desconto+"&tipo="+tipo;
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Cupom criado', displayLength : 2000});
                                document.getElementById("tb_cupons").innerHTML = location.reload();
                            }
                            else
                                M.toast({html: 'Erro ao inserir cupom', displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
            }
        }
        
        function formatarMoeda(inputid) {
            var elemento = document.getElementById(inputid);
            var valor = elemento.value;
            if (valor == null)
                valor = 0.0;
            valor = valor + '';
            valor = parseInt(valor.replace(/[\D]+/g,''));
            valor = valor + '';
            valor = valor.replace(/([0-9]{2})$/g, ",$1");

            if (valor.length > 6) {
                valor = valor.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
            }

            elemento.value = valor;
        }
    </script>
    
    <body>
        <header>
           <?php navBar() ?>
        </header>        
        <div class="container">
            <div class="fixed-action-btn">
                <a class="btn-floating btn-large ddrive waves-effect waves-light modal-trigger" href="#modal_insert">
                    <i class="large material-icons">add</i>
                </a>
            </div>
            <div class="row">
                <h3><center>Cupons</center></h3>
            </div>
            <div class="row" id="tb_cupons" style="overflow-x:auto">
                <table class="striped bordered" style="width:100%">
                    <thead>
                        <th>Nome</th>
                        <th>Código</th>
                        <th>Desconto</th>
                        <th>Tipo</th>
                        <th>Opções</th>
                    </thead>
                    <tbody>
                        <?php 
                            $sql = "SELECT * FROM cupons";
                            $result = mysqli_query($link, $sql);                     
                            while($row = mysqli_fetch_assoc($result)){
                                echo '<tr>';
                                echo '<td>'.$row['nome'].'</td>';
                                echo '<td>'.$row['codigo'].'</td>';
                                echo '<td>'.$row['desconto'].'</td>';
                                echo '<td>'.($row['tipo']=='PORCENTAGEM' ? '%' : 'R$').'</td>';
                                echo '
                                <td>
                                <a class="waves-effect waves-light btn ddrive modal-trigger" href="#modal_edit" onclick="modalEdit(\'update\', '.$row['id'].', \''.$row['nome'].'\', \''.$row['codigo'].'\', \''.$row['desconto'].'\', \''.$row['tipo'].'\')"><i class="material-icons">edit</i></a>
                                <a class="waves-effect waves-light btn red modal-trigger" href="#modal_delete" onclick="modalEdit(\'delete\', '.$row['id'].', \''.$row['nome'].'\', \''.$row['codigo'].'\', \''.$row['desconto'].'\', \''.$row['tipo'].'\')"><i class="material-icons">delete</i></a>
                                </td>';
                                echo '</tr>';
                            }
                            mysqli_close($link);
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        
        <div id="modal_delete" class="modal">
            <div class="modal-content">
                <center>
                    <h4 style="padding-top:10px">Confirmar Exclusão</h4>
                </center>
                <div class="row margin" style="display:none">
                    <div class="input-field col s4">
                        <input id="delid" type="text">
                        <label for="delid" class="active">ID</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="delnome" type="text">
                        <label for="delnome" class="active">Nome</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="delcodigo" type="text">
                        <label for="delcodigo" class="active">Código</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a class="waves-effect waves-light btn ddrive modal-close" onclick="enviar('delete')"><i class="material-icons left">check_circle</i>Sim</a>
                <a class="waves-effect waves-light btn red modal-close"><i class="material-icons left">clear</i>Cancelar</a>
            </div>
        </div>
        
        <div id="modal_edit" class="modal">
            <div class="modal-content">
                <center>
                    <h4 style="padding-top:10px">Editar Usuário</h4>
                </center>
                <div class="row margin" style="display:none">
                    <div class="input-field col s4">
                        <input id="edtid" type="text">
                        <label for="edtid" class="active">ID</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="edtnome" type="text">
                        <label for="edtnome" class="active">Nome</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="edtcodigo" type="text">
                        <label for="edtcodigo" class="active">Código</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="edtdesconto" type="text" pattern="[0-9]*" onkeyup="formatarMoeda('edtdesconto')">
                        <label class="active" for="edtdesconto">Desconto</label>
                    </div>
                </div> 
                <div class="row margin">
                    <div class="input-field col s6 m2">
                        <select id="edttipo" >
                            <option value="PORCENTAGEM" selected>%</option>
                            <option value="REAIS">R$</option>
                        </select>
                    </div>
                </div>   
            </div>                            
            <div class="modal-footer">
                <a class="waves-effect waves-light btn ddrive modal-close" onclick="enviar('update')"><i class="material-icons left">save</i>Alterar</a>
                <a class="waves-effect waves-light btn red modal-close"><i class="material-icons left">clear</i>Cancelar</a>
            </div>
        </div>
        
        <div id="modal_insert" class="modal">
            <div class="modal-content">
                <center>
                    <h4 style="padding-top:10px">Inserir Operador</h4>
                </center>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="addnome" type="text">
                        <label for="addnome" class="active">Nome</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="addcodigo" type="text">
                        <label for="addcodigo" class="active">Código</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="adddesconto" type="text" pattern="[0-9]*" onkeyup="formatarMoeda('adddesconto')">
                        <label class="active" for="adddesconto">Desconto</label>
                    </div>
                </div>                
                <div class="row margin">
                    <div class="input-field col s6 m2">
                        <select id="addtipo" >
                            <option value="PORCENTAGEM" selected>%</option>
                            <option value="REAIS">R$</option>
                        </select>
                    </div>
                </div>   
            </div>              
            <div class="modal-footer">
                <a class="waves-effect waves-light btn ddrive modal-close" onclick="enviar('insert')"><i class="material-icons left">save</i>Salvar</a>
                <a class="waves-effect waves-light btn red modal-close"><i class="material-icons left">clear</i>Cancelar</a>
            </div>
        </div>
        
        <div id="modal_loading" class="modal">
             <center>
                <div class="modal-content">
                    <div class="preloader-wrapper big active">
                        <div class="spinner-layer spinner-blue-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                                <div class="circle"></div>
                            </div><div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                 </div>
             </center>
        </div>

    </body>
</html>
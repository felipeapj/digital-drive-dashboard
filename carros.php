<?php 
    setlocale(LC_ALL, 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Digital Drive</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link rel = "stylesheet" href = "https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="css/materialize.css">
        <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script> 
        <meta charset="UTF-8">
    </head>

    <script type="text/javascript" language="javascript">
        var mdLoading;
        var autocomplete_el;
        var modelos_str = "";
        var selectMarcas;
        $(document).ready(function(){
            $('.modal').modal();
            $('select').formSelect();
            $('input.autocomplete').autocomplete();
            mdLoading = M.Modal.getInstance(document.getElementById("modal_loading"));
            autocomplete_el = M.Autocomplete.getInstance(document.getElementById('modelo'));
            $('.sidenav').sidenav({
                menuWidth: 200, 
                edge: 'right', 
                closeOnClick: false, 
                draggable: true 
            });
            listarMarcas();
        });
        
        function listarMarcas(){
            selectMarcas = document.getElementById('marcas');
            var url = "http://fipeapi.appspot.com/api/1/carros/marcas.json";
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "GET", url, true );
            xmlHttp.onreadystatechange = function() {
                if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                    var marcas = JSON.parse(xmlHttp.responseText);
                    marcas.forEach(popularMarcas);  
                    $('select').formSelect();
                }
            }
            xmlHttp.send( null );
        }
        
        function popularMarcas(value, index, arr){
            var marca = value['fipe_name'];
            var id = value['id']
            var option = document.createElement("option");
            option.value = id;
            option.text = marca;
            selectMarcas.add(option);
        }
        
        function listarModelos(){
            var id = document.getElementById("marcas").value;
            var url = "http://fipeapi.appspot.com/api/1/carros/veiculos/"+id+".json";
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "GET", url, false );
            xmlHttp.send( null );
            var modelos = JSON.parse(xmlHttp.responseText);
            modelos_str = '{ ';
            modelos.forEach(modelosAutoComplete);
            modelos_str = modelos_str + "}";
            autocomplete_el.updateData(JSON.parse(modelos_str));
        }
        
        function modelosAutoComplete(value, index, arr){
            var nome = value['fipe_name'];
            if (index == arr.length-1)
                modelos_str = modelos_str + "\"" + nome + "\":null ";
            else
                modelos_str = modelos_str + "\"" + nome + "\":null, ";
        }
        
        function enviar(op){
            var http = new XMLHttpRequest();
            var url = 'carController.php';
            mdLoading.open({opacity:1});
            http.open('POST', url, true);
            http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            switch (op){
                case 'insertmarca':
                    var params = "op=insert&data="+toJSONString(document.getElementById('cadastrar'));
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();                            
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Veículo cadastrado', displayLength : 2000});                               
                            }
                            else
                                M.toast({html: 'Erro ao cadastrar veículo', displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
                    
                case 'delete':
                    var params = "op=delete&id="+document.getElementById('delid').value;
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();
                            console.log(http.responseText);
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Estabelecimento deletado', displayLength : 2000});
                                document.getElementById("estabelecimentos").innerHTML = location.reload();
                            }
                            else
                                M.toast({html: 'Erro ao deletar estabelecimento', displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
                case 'update':
                    break;
            }
        }
        
        function toJSONString( form ) {
            var obj = {};
            var elements = form.querySelectorAll( "input, select" );
            for( var i = 0; i < elements.length; ++i ) {
                var element = elements[i];
                var name = element.name;
                var value = element.value;
                if( name ) {
                    obj[ name ] = value;
                }
            }
            return JSON.stringify( obj );
	   }
    </script>
    
    <body>
	   <div class="container">
           <div class="row">
               <h3><center>Carros</center></h3>
           </div>            
           <div class="row margin">
               <a class="waves-effect waves-light btn ddrive modal-trigger" href="#modal_inserir"><i class="material-icons right">add_circle_outline</i>Adicionar</a>
           </div>
           <div class="row" id="tabela">
               <table class="striped bordered" style="width:100%">
                   <thead>
                       <th>Marca</th>
                       <th>Modelo</th>
                       <th>Ano</th>
                       <th>Opções</th>
                   </thead>
                   <tbody>
                   </tbody>
               </table>
           </div>
        </div>
        
        <div id="modal_inserir" class="modal modal modal-fixed-footer">
            <div class="modal-content">
                <center><h4 style="padding-top:10px">Inserir Veículo</h4></center>
                <form id="cadastrar" action="#!" method="post">
                    <div class="row margin">
                        <div class="input-field col s6 m3">
                            <select id="marcas" class="materialSelect" name="marca" onchange="listarModelos()">
                                <option value="" disabled selected>Fabricante</option>
                                
                            </select>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s10 m5">
                            <input type="text" id="modelo" name="modelo" class="autocomplete">
                            <label for="modelo">Modelo</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s4 m2">
                            <input type="number" step="1" name="anofabricacao" id="anofabricacao" value="<?php echo date('Y') ?>">
                            <label for="anofabricacao">Fabricação</label>
                        </div>
                        <div class="input-field col s4 m2">
                            <input type="number" step="1" name="anomodelo" id="anomodelo" value="<?php echo date('Y') ?>">
                            <label for="anomodelo">Modelo</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s6 m3">
                            <input type="text" id="placa" name="placa" style="text-transform:uppercase">
                            <label for="placa">Placa</label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">  
                <a class="waves-effect waves-light btn materialize-red modal-close"><i class="material-icons left">reply</i>Cancelar</a>
                <button class="btn waves-effect waves-light modal-close" type="submit" name="action" onclick="enviar('insert')"><i class="material-icons right">save</i>Salvar</button>
            </div>        
        </div>
                
        <div id="modal_loading" class="modal">
             <center>
                <div class="modal-content">
                    <div class="preloader-wrapper big active">
                        <div class="spinner-layer spinner-blue-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                                <div class="circle"></div>
                            </div><div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                 </div>
             </center>
        </div>
</body>
</html>
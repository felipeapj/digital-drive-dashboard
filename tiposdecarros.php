<?php 
    session_start();
    include_once 'connect.php';
    if(!isset ($_SESSION['logado']))
    	header('location:index.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Digital Drive</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link rel = "stylesheet" href = "https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="css/materialize.css">
        <link rel="stylesheet" href="css/style.css">
        <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <meta charset="utf8">
    </head>
    
    <script type="text/javascript" language="javascript">
        var mdLoading;
        $(document).ready(function(){
            $('.fixed-action-btn').floatingActionButton();
            $('.modal').modal();
            $('.materialboxed').materialbox();
            $('.sidenav').sidenav({
                edge: 'left', // Choose the horizontal origin
                closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
                draggable: true
            });            
            $('.collapsible').collapsible();
            mdLoading = M.Modal.getInstance(document.getElementById('modal_loading'));
        });

        function modalEdit(op, id, category, category_slug, icon, coast){
            if (op=="delete"){
                document.getElementById('delid').value = id;
                document.getElementById('delcategory').value = category;
            }
            else {
                document.getElementById('edtid').value = id;
                document.getElementById('edtcategory').value = category;
                document.getElementById('edtcategory_slug').value = category_slug;
                document.getElementById('edticon').value = icon;
                document.getElementById('edtcoast').value = coast;
            }
            M.updateTextFields();
        }
        
        function enviar(op){            
            var http = new XMLHttpRequest();
            var url = 'tiposdecarroscontroller.php';
            mdLoading.open({opacity:1});
            http.open('POST', url, true);
            http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            switch (op){
                case 'delete':
                    var id = document.getElementById('delid').value;
                    var params = "op=delete&id="+id;                    
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Categoria deletada', displayLength : 2000});
                                document.getElementById("tb_categorias").innerHTML = location.reload();
                            }
                            else
                                M.toast({html: 'Erro ao deletar categoria', displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
                case 'update':
                    var id = document.getElementById('edtid').value;
                    var category = document.getElementById('edtcategory').value;
                    var category_slug = document.getElementById('edtcategory_slug').value;
                    var icon = document.getElementById('edticon').value;
                    var coast = document.getElementById('edtcoast').value;
                    var params = "op=update&id="+id+"&category="+category+"&category_slug="+category_slug+"&icon="+icon+"&coast="+coast;
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Categoria alterada', displayLength : 2000});
                                document.getElementById("tb_categorias").innerHTML = location.reload();
                            }
                            else
                                M.toast({html: 'Erro ao alterar categoria', displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
                case 'insert':
                    var category = document.getElementById('addcategory').value;
                    var category_slug = document.getElementById('addcategory_slug').value;
                    var icon = document.getElementById('addicon').value;
                    var coast = document.getElementById('addcoast').value;
                    var params = "op=insert&category="+category+"&category_slug="+category_slug+"&icon="+icon+"&coast="+coast;
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Categoria criada', displayLength : 2000});
                                document.getElementById("tb_categorias").innerHTML = location.reload();
                            }
                            else
                                M.toast({html: 'Erro ao inserir categoria - '+http.responseText, displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
            }
        }
        
        function formatarMoeda(inputid) {
            var elemento = document.getElementById(inputid);
            var valor = elemento.value;
            if (valor == null)
                valor = 0.0;
            valor = valor + '';
            valor = parseInt(valor.replace(/[\D]+/g,''));
            valor = valor + '';
            valor = valor.replace(/([0-9]{2})$/g, ",$1");

            if (valor.length > 6) {
                valor = valor.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
            }

            elemento.value = valor;
        }
    </script>
    
    <body>
        <header>
           <?php navBar() ?>
        </header>        
        <div class="container">
            <div class="fixed-action-btn">
                <a class="btn-floating btn-large ddrive waves-effect waves-light modal-trigger" href="#modal_insert">
                    <i class="large material-icons">add</i>
                </a>
            </div>
            <div class="row">
                <h3><center>Tipos de veículos</center></h3>
            </div>
            <div class="row" id="tb_categorias" style="overflow-x:auto">
                <table class="striped bordered" style="width:100%">
                    <thead>
                        <th>Categoria</th>
                        <th>Slug</th>
                        <th>Icon</th>
                        <th>Custo</th>
                        <th>Opções</th>
                    </thead>
                    <tbody>
                        <?php 
                            $sql = "SELECT * FROM categories";
                            $result = mysqli_query($link, $sql);                     
                            while($row = mysqli_fetch_assoc($result)){
                                echo '<tr>';
                                echo '<td>'.$row['category'].'</td>';
                                echo '<td>'.$row['category_slug'].'</td>';
                                echo '<td><img class="materialboxed" width="50" src="'.$row['icon'].'" alt="'.$row['icon'].'"></td>';
                                echo '<td>'.$row['coast'].'</td>';
                                echo '
                                <td>
                                <a class="waves-effect waves-light btn ddrive modal-trigger" href="#modal_edit" onclick="modalEdit(\'update\', '.$row['id'].', \''.$row['category'].'\', \''.$row['category_slug'].'\', \''.$row['icon'].'\', '.$row['coast'].')"><i class="material-icons">edit</i></a>
                                <a class="waves-effect waves-light btn red modal-trigger" href="#modal_delete" onclick="modalEdit(\'delete\', '.$row['id'].', \''.$row['category'].'\', \''.$row['category_slug'].'\', \''.$row['icon'].'\', '.$row['coast'].')"><i class="material-icons">delete</i></a>
                                </td>';
                                echo '</tr>';
                            }
                            mysqli_close($link);
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        
        <div id="modal_delete" class="modal">
            <div class="modal-content">
                <center>
                    <h4 style="padding-top:10px">Confirmar Exclusão</h4>
                </center>
                <div class="row margin" style="display:none">
                    <div class="input-field col s4">
                        <input id="delid" type="text">
                        <label for="delid" class="active">ID</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="delcategory" type="text" readonly>
                        <label for="delcategory" class="active">Categoria</label>
                    </div>
                </div>   
            </div>      
            <div class="modal-footer">
                <a class="waves-effect waves-light btn ddrive modal-close" onclick="enviar('delete')"><i class="material-icons left">check_circle</i>Excluir</a>
                <a class="waves-effect waves-light btn red modal-close"><i class="material-icons left">clear</i>Cancelar</a>
            </div>
        </div>
        
        <div id="modal_edit" class="modal">
            <div class="modal-content">
                <center>
                    <h4 style="padding-top:10px">Editar Categoria</h4>
                </center>
                <div class="row margin" style="display:none">
                    <div class="input-field col s4">
                        <input id="edtid" type="text">
                        <label for="edtid" class="active">ID</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s10 m3">
                        <input id="edtcategory" type="text">
                        <label for="edtcategory" class="active">Categoria</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s10 m3">
                        <input id="edtcategory_slug" type="text">
                        <label for="edtcategory_slug" class="active">Slug</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12 m8">
                        <input id="edticon" type="text">
                        <label class="active" for="edticon">Icon</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6 m2">
                        <input id="edtcoast" type="text" pattern="[0-9]*" onkeyup="formatarMoeda('edtcoast')">
                        <label class="active" for="edtcoast">Custo</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a class="waves-effect waves-light btn ddrive modal-close" onclick="enviar('update')"><i class="material-icons left">save</i>Alterar</a>
                <a class="waves-effect waves-light btn red modal-close"><i class="material-icons left">clear</i>Cancelar</a>
            </div>
        </div>
        
        <div id="modal_insert" class="modal">
            <div class="modal-content">
                <center>
                    <h4 style="padding-top:10px">Inserir Categoria</h4>
                </center>
                <div class="row margin">
                    <div class="input-field col s10 m3">
                        <input id="addcategory" type="text">
                        <label for="addcategory" class="active">Categoria</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s10 m3">
                        <input id="addcategory_slug" type="text">
                        <label for="addcategory_slug" class="active">Slug</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12 m8">
                        <input id="addicon" type="text">
                        <label class="active" for="addicon">Icon</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6 m2">
                        <input id="addcoast" type="text" pattern="[0-9]*" onkeyup="formatarMoeda('addcoast')">
                        <label class="active" for="addcoast">Custo</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a class="waves-effect waves-light btn ddrive modal-close" onclick="enviar('insert')"><i class="material-icons left">save</i>Salvar</a>
                <a class="waves-effect waves-light btn red modal-close"><i class="material-icons left">clear</i>Cancelar</a>
            </div>
        </div>
        
        <div id="modal_loading" class="modal">
             <center>
                <div class="modal-content">
                    <div class="preloader-wrapper big active">
                        <div class="spinner-layer spinner-blue-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                                <div class="circle"></div>
                            </div><div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                 </div>
             </center>
        </div>

    </body>
</html>
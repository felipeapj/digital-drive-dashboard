<?php
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL);
	include_once('connect.php');
	header("Content-Type: application/json; charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT, GET, POST");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	$inputJSON = file_get_contents('php://input');
	$postdata = json_decode($inputJSON);
	$number = $postdata->telefone;
	
	$sql = "SELECT * FROM rider WHERE mobile_number = ".$number;
	$consulta = $conn->query($sql);
	$consulta = $consulta->fetchAll();
	if(count($consulta) > 0){
		$retorno = array(
			'response' => array(
				'error' => 'true',
				'message' => "Numero de celular j� registrado!"
			)
		);

		echo json_encode($retorno);
	
	}else{
	
		$token_fcm = $postdata->token;
		$first_name = $postdata->nome;
		$last_name = $postdata->sobrenome;
		$status = 1;
		$password = $postdata->senha;
		$image_address = $postdata->image_address;
		$birth_date = $postdata->data_nascimento;
		$email = $postdata->email;
		$gender = $postdata->sexo;
		$address = $postdata->endereco;

		$org = $postdata->org_militar;
		$cargo = $postdata->cargo_militar;

		$data = base64_decode($image_address);
		$img_name = time().'.jpg';
		$path = 'imgs/'.$img_name;
		file_put_contents($path, $data);


		$sql = 'INSERT INTO rider (first_name,last_name,mobile_number,password,birth_date,image_address,email,gender,address,token,org_militar,cargo_militar) VALUES("'.$first_name.'","'.$last_name.'",'.$number.',"'.$password.'","'.$birth_date.'","'.$img_name.'","'.$email.'","'.$gender.'","'.$address.'","'.$token_fcm.'", '.$org.', '.$cargo.')';
		$conn->query($sql);

		$id = $conn->lastInsertId();
		$retorno = array(
			'response' => array(
				'error' => 'false',
				'idRider' => $id
			)
		);

		echo json_encode($retorno);
	}
?>
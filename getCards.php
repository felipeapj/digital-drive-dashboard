<?php
	include_once('connect.php');
	header("Content-Type: application/json; charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT, GET, POST");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	$postdata = json_decode(base64_decode($_GET['data']));
	$sql = 'SELECT id,numero FROM credit_cards WHERE id_rider = '.$postdata->idRider.'';
	$consulta = $conn->prepare($sql);
    $consulta->execute();

    $result = $consulta->fetchAll(PDO::FETCH_ASSOC);

    $cards = array();

    $cards = array_map(function($card){
    	$n_tmp = base64_decode( $card['numero'] );
    	unset($card['numero']);
    	$n_tmp = "**** ".substr($n_tmp, -4);
    	$card['cardNumber'] = $n_tmp;
    	return $card;
    }, $result);

    $retorno = array(
    	'response' => $cards
    );

    echo json_encode($retorno);
?>
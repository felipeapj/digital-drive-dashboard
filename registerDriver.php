<?php
	include_once('connect.php');
	header("Content-Type: application/json; charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT, GET, POST");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	$inputJSON = file_get_contents('php://input');
	$postdata = json_decode($inputJSON);
	
		$token_fcm = $postdata->token;
		$number = $postdata->telefone;
		$first_name = $postdata->nome;
		$last_name = $postdata->sobrenome;
		$status = 1;
		$password = $postdata->senha;
		$image_address = $postdata->image_address;
		$birth_date = $postdata->data_nascimento;
		$email = $postdata->email;
		$gender = $postdata->sexo;
		$address = $postdata->endereco;
        $rg = $postdata->rg;
        $cnh = $postdata->cnh;
        
        $rg_print_data = $postdata->rg_print;
		$rg_print = "RG_".time().'.jpg';
		$path = 'imgs/'.$rg_print;
		file_put_contents($path, $rg_print_data);
		
		
        $cnh_print_data = $postdata->cnh_print;
		$cnh_print = "CNH_".time().'.jpg';
		$path = 'imgs/'.$cnh_print;
		file_put_contents($path, $cnh_print_data);
        
        $modelo_carro = $postdata->carro_modelo;
		$modelo_carro = 0;
		$car_type = $postdata->carro_tipo;
		$placa_carro = $postdata->carro_placa;
		$cor_carro = $postdata->carro_cor;
		$ano_carro = $postdata->carro_ano;

		$data = base64_decode($image_address);
		$img_name = time().'.jpg';
		$path = 'imgs/'.$img_name;
		file_put_contents($path, $data);


		$sql = 'INSERT INTO driver (first_name,last_name,mobile_number,password,birth_date,driver_image,email,gender,address,token,fk_car_id, car_color, car_board,rg,cnh,rg_img,cnh_img,car_production_year) VALUES("'.$first_name.'","'.$last_name.'",'.$number.',"'.$password.'","'.$birth_date.'","'.$img_name.'","'.$email.'","'.$gender.'","'.$address.'","'.$token_fcm.'",'.$car_type.','.$cor_carro.',"'.$placa_carro.'","'.$rg.'","'.$cnh.'","'.$rg_print.'","'.$cnh_print.'","'.$ano_carro.'")';
		$cadastra = $conn->prepare($sql);
		
		if (!$cadastra->execute()) {
			print_r($cadastra->errorInfo());
		}else{

			$id = $conn->lastInsertId();
			$retorno = array(
				'response' => array(
					'error' => 'false',
					'idRider' => $id
				)
			);

			echo json_encode($retorno);
		}
	
	
?>
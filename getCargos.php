<?php
	include_once('connect.php');
	header("Content-Type: application/json; charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT, GET, POST");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	$postdata = json_decode(base64_decode($_GET['data']));
	$id = $postdata->id;
	$sql = 'SELECT * FROM military_graduation WHERE org_id = '.$id;
	$consulta = $conn->prepare($sql);
    $consulta->execute();

    $result = $consulta->fetchAll(PDO::FETCH_ASSOC);

    $retorno = array(
    	'response' => $result
    );

    echo json_encode($retorno);
?>
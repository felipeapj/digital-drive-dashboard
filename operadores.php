<?php 
    session_start();
    include_once 'connect.php';
    if(!isset ($_SESSION['logado']))
    	header('location:index.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Digital Drive</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link rel = "stylesheet" href = "https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="css/materialize.css">
        <link rel="stylesheet" href="css/style.css">
        <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <link href="https://unpkg.com/tabulator-tables@4.2.4/dist/css/tabulator.min.css" rel="stylesheet">
        <script type="text/javascript" src="https://unpkg.com/tabulator-tables@4.2.4/dist/js/tabulator.min.js"></script>
        <meta charset="utf8">
    </head>
    
    <script type="text/javascript" language="javascript">
        var mdLoading;
        var op;
        var table;
        $(document).ready(function(){
            $('.fixed-action-btn').floatingActionButton();
            $('.modal').modal();
            $('select').formSelect();
            $('.sidenav').sidenav({
                edge: 'left', 
                closeOnClick: false,
                draggable: true
            });            
            $('.collapsible').collapsible();
            mdLoading = M.Modal.getInstance(document.getElementById('modal_loading'));            
            var printIcon = function(cell, formatterParams, onRendered){
                return '<a class="waves-effect waves-light btn green modal-trigger" href="#modal_detail" onclick="changeOp(\'detail\')"><i class="material-icons">visibility</i></a><a class="waves-effect waves-light btn ddrive modal-trigger" href="#modal_edit" onclick="changeOp(\'update\')"><i class="material-icons">edit</i></a><a class="waves-effect waves-light btn red modal-trigger" href="#modal_delete" onclick="changeOp(\'delete\')"><i class="material-icons">delete</i></a>';
            };
            table = new Tabulator("#tb_operadores", {
                ajaxURL:"operadorescontroller.php",
                ajaxConfig:{
                    method:"POST", 
                    headers: {
                        "Content-type": 'application/x-www-form-urlencoded'
                    },
                },
                pagination:"local",
                paginationSize:10,
                columns:[
                    {title:"Nome", field:"first_name"},
                    {title:"Sobrenome", field:"last_name"},
                    {title:"Email", field:"email"},
                    {title:"Senha", field:"password"},
                    {title:"Opções", formatter:printIcon, align:"center", cellClick:function(e, cell){
                        var data = cell.getRow().getData();
                        modalEdit(data.id, data.first_name, data.last_name, data.image_address, data.email, data.password, data.type, data.mobile_number, data.phone_number, data.address, data.access_base_types, data.access_stats, data.access_users, data.access_drivers, data.access_travels, data.access_complaints, data.access_call_requests, data.access_payment_requests, data.is_revoked);
                    }}
                ],
                layout:"fitColumns",
            });
        });
        
        function changeOp(newOp){
            op = newOp;
        }
        function modalEdit(id, first_name, last_name, image_address, email, password, type, mobile_number, phone_number, address, access_base_types, access_stats, access_users, access_drivers, access_travels, access_complaints, access_call_requests, access_payment_requests, is_revoked){
            switch (op){
                case 'delete':
                    document.getElementById('delid').value = id;
                    document.getElementById('delnome').value = first_name;
                    document.getElementById('delsobrenome').value = last_name;
                    break;
                case 'update':
                    document.getElementById('edtid').value = id;
                    document.getElementById('edtfirst_name').value = first_name;
                    document.getElementById('edtlast_name').value = last_name;
                    document.getElementById('edtimage_address').value = image_address;
                    document.getElementById('edtemail').value = email;
                    document.getElementById('edtpassword').value = password;
                    document.getElementById('edttype').value = type;
                    document.getElementById('edtmobile_number').value = mobile_number;
                    document.getElementById('edtphone_number').value = phone_number;
                    document.getElementById('edtaddress').value = address;
                    document.getElementById('edtaccess_base_types').checked = (access_base_types==1 ? true : false);
                    document.getElementById('edtaccess_stats').checked = (access_stats==1 ? true : false);
                    document.getElementById('edtaccess_users').checked = (access_users==1 ? true : false);
                    document.getElementById('edtaccess_drivers').checked = (access_drivers==1 ? true : false);
                    document.getElementById('edtaccess_travels').checked = (access_travels==1 ? true : false);
                    document.getElementById('edtaccess_complaints').checked = (access_complaints==1 ? true : false)
                    document.getElementById('edtaccess_call_requests').checked = (access_call_requests==1 ? true : false);
                    document.getElementById('edtaccess_payment_requests').checked = (access_payment_requests==1 ? true : false);
                    document.getElementById('edtis_revoked').checked = (is_revoked==1 ? true : false);
                    break;
                case 'detail':
                    document.getElementById('dtfirst_name').innerHTML = first_name;
                    document.getElementById('dtlast_name').innerHTML = last_name;
                    document.getElementById('dtimage_address').source = image_address;
                    document.getElementById('dtimage_address').alt = image_address;
                    document.getElementById('dtemail').innerHTML = email;
                    document.getElementById('dtpassword').innerHTML = password;
                    document.getElementById('dttype').innerHTML = type;
                    document.getElementById('dtmobile_number').innerHTML = mobile_number;
                    document.getElementById('dtphone_number').innerHTML = phone_number;
                    document.getElementById('dtaddress').innerHTML = address;
                    document.getElementById('dtaccess_base_types').innerHTML = (access_base_types==1 ? 'SIM' : 'NÃO');
                    document.getElementById('dtaccess_stats').innerHTML = (access_stats==1 ? 'SIM' : 'NÃO');
                    document.getElementById('dtaccess_users').innerHTML = (access_users==1 ? 'SIM' : 'NÃO');
                    document.getElementById('dtaccess_drivers').innerHTML = (access_drivers==1 ? 'SIM' : 'NÃO');
                    document.getElementById('dtaccess_travels').innerHTML = (access_travels==1 ? 'SIM' : 'NÃO');
                    document.getElementById('dtaccess_complaints').innerHTML = (access_complaints==1 ? 'SIM' : 'NÃO')
                    document.getElementById('dtaccess_call_requests').innerHTML = (access_call_requests==1 ? 'SIM' : 'NÃO');
                    document.getElementById('dtaccess_payment_requests').innerHTML = (access_payment_requests==1 ? 'SIM' : 'NÃO');
                    document.getElementById('dtis_revoked').innerHTML = (is_revoked==1 ? 'SIM' : 'NÃO');
            }
            M.updateTextFields();
        }
        
        function mascara(o,f){
            v_obj=o
            v_fun=f
            setTimeout("execmascara()",1)
        }
    
           
        function execmascara(){
            v_obj.value=v_fun(v_obj.value)
        }
        
        function mtel(v){
            v=v.replace(/\D/g,"");            
            v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); 
            v=v.replace(/(\d)(\d{4})$/,"$1-$2"); 
            return v;
        }
        
        function enviar(op){            
            var http = new XMLHttpRequest();
            var url = 'operadorescontroller.php';
            mdLoading.open({opacity:1});
            http.open('POST', url, true);
            http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            switch (op){
                case 'delete':
                    var id = document.getElementById('delid').value;
                    var params = "op=delete&id="+id;                    
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Operador deletado', displayLength : 2000});
                                table.setData();
                                $(':input').val('');
                            }
                            else
                                M.toast({html: 'Erro ao deletar operador', displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
                case 'insert':
                    var first_name = document.getElementById('addfirst_name').value;
                    var last_name = document.getElementById('addlast_name').value;
                    var image_address = document.getElementById('addimage_address').value;
                    var email = document.getElementById('addemail').value;
                    var password = document.getElementById('addpassword').value;
                    var type = document.getElementsByTagName('type').value;
                    var mobile_number = document.getElementById('addmobile_number').value;
                    var phone_number = document.getElementById('addphone_number').value;
                    var address = document.getElementById('addaddress').value;
                    var access_base_types = document.getElementById('addaccess_base_types').checked;
                    var access_stats = document.getElementById('addaccess_stats').checked;
                    var access_users = document.getElementById('addaccess_users').checked;
                    var access_drivers = document.getElementById('addaccess_drivers').checked;
                    var access_travels = document.getElementById('addaccess_travels').checked;
                    var access_complaints = document.getElementById('addaccess_complaints').checked;
                    var access_call_requests = document.getElementById('addaccess_call_requests').checked;
                    var access_payment_requests = document.getElementById('addaccess_payment_requests').checked;
                    var is_revoked = document.getElementById('addis_revoked').checked;
                    var params = "op=insert&first_name="+first_name+"&last_name="+last_name+"&image_address="+image_address+"&email="+email+"&password="+password+"&type="+type+"&mobile_number="+mobile_number+"&phone_number="+phone_number+"&address="+address+"&access_base_types="+access_base_types+"&access_stats="+access_stats+"&access_users="+access_users+"&access_drivers="+access_drivers+"&access_travels="+access_travels+"&access_complaints="+access_complaints+"&access_call_requests="+access_call_requests+"&access_payment_requests="+access_payment_requests+"&is_revoked="+is_revoked;
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Operador inserido', displayLength : 2000});
                                table.setData();
                                $(':input').val('');
                            }
                            else
                                M.toast({html: 'Erro ao inserir operador - '+http.responseText, displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
                case 'update':
                    var id = document.getElementById('edtid').value;
                    var first_name = document.getElementById('edtfirst_name').value;
                    var last_name = document.getElementById('edtlast_name').value;
                    var image_address = document.getElementById('edtimage_address').value;
                    var email = document.getElementById('edtemail').value;
                    var password = document.getElementById('edtpassword').value;
                    var type = document.getElementsByTagName('type').value;
                    var mobile_number = document.getElementById('edtmobile_number').value;
                    var phone_number = document.getElementById('edtphone_number').value;
                    var address = document.getElementById('edtaddress').value;
                    var access_base_types = document.getElementById('edtaccess_base_types').checked;
                    var access_stats = document.getElementById('edtaccess_stats').checked;
                    var access_users = document.getElementById('edtaccess_users').checked;
                    var access_drivers = document.getElementById('edtaccess_drivers').checked;
                    var access_travels = document.getElementById('edtaccess_travels').checked;
                    var access_complaints = document.getElementById('edtaccess_complaints').checked;
                    var access_call_requests = document.getElementById('edtaccess_call_requests').checked;
                    var access_payment_requests = document.getElementById('edtaccess_payment_requests').checked;
                    var is_revoked = document.getElementById('edtis_revoked').checked;
                    var params = "op=update&id="+id+"&first_name="+first_name+"&last_name="+last_name+"&image_address="+image_address+"&email="+email+"&password="+password+"&type="+type+"&mobile_number="+mobile_number+"&phone_number="+phone_number+"&address="+address+"&access_base_types="+access_base_types+"&access_stats="+access_stats+"&access_users="+access_users+"&access_drivers="+access_drivers+"&access_travels="+access_travels+"&access_complaints="+access_complaints+"&access_call_requests="+access_call_requests+"&access_payment_requests="+access_payment_requests+"&is_revoked="+is_revoked;
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Operador atualizado', displayLength : 2000});
                                table.setData();
                                $(':input').val('');
                            }
                            else{
                                M.toast({html: 'Erro ao atualizar operador', displayLength : 2000});
                                console.log(http.responseText);
                            }
                        }
                    }                    
                    http.send(params);
                    break;
            }
        }
        
        function filtrar(){
            table.clearFilter();
            var campo = document.getElementById('campo').value;
            var operador = document.getElementById('operador').value;
            var filtro = document.getElementById('filtro').value;
            table.setFilter(campo, operador, filtro);
        }
    </script>
    
    <body>
        <header>
           <?php navBar() ?>
        </header>        
        <div class="container">
            <div class="fixed-action-btn">
                <a class="btn-floating btn-large ddrive waves-effect waves-light modal-trigger" href="#modal_insert">
                    <i class="large material-icons">add</i>
                </a>
            </div>
            <div class="row">
                <h3><center>Operadores</center></h3>
            </div>
            <div class="row valign-wrapper">
                <div class="input-field col s6 m3">
                    <select id="campo">
                        <option value="first_name" selected>Nome</option>
                        <option value="last_name">Sobrenome</option>
                        <option value="image_address">Imagem</option>
                        <option value="email">Email</option>
                        <option value="password">Senha</option>
                        <option value="type">Tipo</option>
                        <option value="mobile_number">Tel. Móvel</option>
                        <option value="phone_number">Tel. Fixo</option>
                        <option value="address">Endereço</option>
                        <option value="access_base_types">Acesso Base Tipos</option>
                        <option value="access_stats">Acesso stats</option>
                        <option value="access_users">Acesso Usuários</option>
                        <option value="	access_drivers">Acesso Motoristas</option>
                        <option value="access_travels">Acesso Viagens</option>
                        <option value="access_complaints">Acesso Reclamações</option>
                        <option value="access_call_requests">Acesso Chamadas</option>
                        <option value="access_payment_requests">Acesso pagamentos</option>
                        <option value="is_revoked">Revogado</option>
                    </select>
                    <label>Campo</label>
                </div>
                <div class="input-field col s4 m2">
                    <select id="operador">
                        <option value="=" selected>Igual</option>
                        <option value="!=">Diferente</option>
                        <option value="like">Contendo</option>
                        <option value="<">Menor</option>
                        <option value="<=">Menor ou Igual</option>
                        <option value=">">Maior</option>
                        <option value=">=">Maior ou igual</option>
                    </select>
                    <label>Comparação</label>
                </div>
                <div class="input-field col s10 m5">
                    <input id="filtro" type="text" class="validate">
                    <label for="filtro">Valor</label>
                </div>
                <a class="waves-effect waves-light ddrive btn" onclick="filtrar()"><i class="material-icons right">filter_list</i>Filtrar</a>
            </div>
            <div class="row" id="tb_operadores"></div>
        </div>
        
        <div id="modal_delete" class="modal">
            <div class="modal-content">
                <center>
                    <h4 style="padding-top:10px">Confirmar Exclusão</h4>
                    <div class="row margin" style="display:none">
                        <div class="input-field col s4 m2">
                            <input id="delid" type="text" readonly>
                            <label for="delid" class="active">ID</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s6 m4">
                            <input id="delnome" type="text" readonly>
                            <label for="delnome" class="active">Nome</label>
                        </div>
                        <div class="input-field col s6 m4">
                            <input id="delsobrenome" type="text" readonly>
                            <label for="delsobrenome" class="active">Sobrenome</label>
                        </div>
                    </div>                    
                </center>
            </div>
            <div class="modal-footer">
                <a class="waves-effect waves-light btn ddrive modal-close" onclick="enviar('delete')"><i class="material-icons left">check_circle</i>Sim</a>
                <a class="waves-effect waves-light btn red modal-close"><i class="material-icons left">clear</i>Não</a>
            </div>
        </div>
        
        <div id="modal_edit" class="modal">
            <div class="modal-content">
                <center>
                    <h4 style="padding-top:10px">Editar Operador</h4>                    
                </center>
                <div class="row margin" style="display:none">
                    <div class="input-field col s4">
                        <input id="edtid" type="text">
                        <label for="edtid" class="active">ID</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="edtfirst_name" type="text">
                        <label for="edtfirst_name" class="active">Nome</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="edtlast_name" type="text">
                        <label for="edtlast_name" class="active">Sobrenome</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="edtimage_address" type="text">
                        <label for="edtimage_address" class="active">Caminho da imagem</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="edtemail" type="email">
                        <label class="active" for="edtemail">Email</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="edtpassword" type="text">
                        <label class="active" for="edtpassword">Senha</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s5 m1">
                        <input id="edttype" type="text">
                        <label class="active" for="edttype">Tipo</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="edtmobile_number" type="text" onkeyup="mascara( this, mtel );" maxlength="15">
                        <label class="active" for="edtmobile_number">Telefone móvel</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="edtphone_number" type="text" onkeyup="mascara( this, mtel );" maxlength="15">
                        <label class="active" for="edtphone_number">Telefone</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="edtaddress" type="text">
                        <label class="active" for="edtaddress">Endereço</label>
                    </div>
                </div>
                <div class="row margin">
                    <p>Acessos:</p>
                </div>
                <div class="row margin">
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="edtaccess_base_types" />
                        <span>Base Types</span>
                    </label>
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="edtaccess_stats" />
                        <span>Stats</span>
                    </label>
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="edtaccess_users" />
                        <span>Users</span>
                    </label>
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="edtaccess_drivers" />
                        <span>Drivers</span>
                    </label>
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="edtaccess_travels" />
                        <span>Travels</span>
                    </label>
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="edtaccess_complaints" />
                        <span>Complaints</span>
                    </label>
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="edtaccess_call_requests" />
                        <span>Call Requests</span>
                    </label>
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="edtaccess_payment_requests" />
                        <span>Payment Requests</span>
                    </label>
                </div>
                <div class="row margin">
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="edtis_revoked" />
                        <span>Revoked</span>
                    </label>
                </div>
            </div>            
            <div class="modal-footer">
                <a class="waves-effect waves-light btn ddrive modal-close" onclick="enviar('update')"><i class="material-icons left">check_circle</i>Aterar</a>
                <a class="waves-effect waves-light btn red modal-close"><i class="material-icons left">clear</i>Cancelar</a>
            </div>
        </div>
        
        <div id="modal_detail" class="modal">
            <div class="modal-content">
                <center><h5 style="padding-top:10px">Detalhes do operador</h5></center>
                <div class="row">
                    <p><b>Nome: </b><p id="dtfirst_name"></p></p>
                </div>
                <div class="row">
                    <p><b>Sobrenome: </b><p id="dtlast_name"></p></p>
                </div>
                <div class="row">
                    <p><b>Imagem: </b><img id="dtimage_address" alt="Imagem" class="materialboxed" width="150" src=""></p>
                </div>
                <div class="row">
                    <p><b>Email: </b><p id="dtemail"></p></p>
                </div>
                <div class="row">
                    <p><b>Senha: </b><p id="dtpassword"></p></p>
                </div>
                <div class="row">
                    <p><b>Tipo: </b><p id="dttype"></p></p>
                </div>
                <div class="row">
                    <p><b>Telefone móvel: </b><p id="dtmobile_number"></p></p>
                </div>
                <div class="row">
                    <p><b>Telefone fixo: </b><p id="dtphone_number"></p></p>
                </div>
                <div class="row">
                    <p><b>Endereço: </b><p id="dtaddress"></p></p>
                </div>
                <div class="row">
                    <p><b>Acesso a base de tipos: </b><p id="dtaccess_base_types"></p></p>
                </div>
                <div class="row">
                    <p><b>Acesso aos stats: </b><p id="dtaccess_stats"></p></p>
                </div>
                <div class="row">
                    <p><b>Acesso aos usuários: </b><p id="dtaccess_users"></p></p>
                </div>
                <div class="row">
                    <p><b>acesso aos motoristas: </b><p id="dtaccess_drivers"></p></p>
                </div>
                <div class="row">
                    <p><b>Acesso às viagens: </b><p id="dtaccess_travels"></p></p>
                </div>
                <div class="row">
                    <p><b>Acesso às reclamações: </b><p id="dtaccess_complaints"></p></p>
                </div>
                <div class="row">
                    <p><b>Acesso às chamadas: </b><p id="dtaccess_call_requests"></p></p>
                </div>
                <div class="row">
                    <p><b>Acesso aos pagamentos: </b><p id="dtaccess_payment_requests"></p></p>
                </div>
                <div class="row">
                    <p><b>Revogado: </b><p id="dtis_revoked"></p></p>
                </div>
            </div>
        </div>
        
        <div id="modal_insert" class="modal">
            <div class="modal-content">
                <center>
                    <h4 style="padding-top:10px">Inserir Operador</h4>                    
                </center>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="addfirst_name" type="text">
                        <label for="addfirst_name" class="active">Nome</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="addlast_name" type="text">
                        <label for="addlast_name" class="active">Sobrenome</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="addimage_address" type="text">
                        <label for="addimage_address" class="active">Caminho da imagem</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="addemail" type="email">
                        <label class="active" for="addemail">Email</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="addpassword" type="text">
                        <label class="active" for="addpassword">Senha</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s5 m1">
                        <input id="addtype" type="text">
                        <label class="active" for="addtype">Tipo</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="addmobile_number" type="text" onkeyup="mascara( this, mtel );" maxlength="15">
                        <label class="active" for="addmobile_number">Telefone móvel</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="addphone_number" type="text" onkeyup="mascara( this, mtel );" maxlength="15">
                        <label class="active" for="addphone_number">Telefone</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="addaddress" type="text">
                        <label class="active" for="addaddress">Endereço</label>
                    </div>
                </div>
                <div class="row margin">
                    <p>Acessos:</p>
                </div>
                <div class="row margin">
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="addaccess_base_types" />
                        <span>Base Types</span>
                    </label>
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="addaccess_stats" />
                        <span>Stats</span>
                    </label>
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="addaccess_users" />
                        <span>Users</span>
                    </label>
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="addaccess_drivers" />
                        <span>Drivers</span>
                    </label>
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="addaccess_travels" />
                        <span>Travels</span>
                    </label>
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="addaccess_complaints" />
                        <span>Complaints</span>
                    </label>
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="addaccess_call_requests" />
                        <span>Call Requests</span>
                    </label>
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="addaccess_payment_requests" />
                        <span>Payment Requests</span>
                    </label>
                </div>
                <div class="row margin">
                    <label style="padding-right:5px">
                        <input type="checkbox" class="filled-in" id="addis_revoked" />
                        <span>Revoked</span>
                    </label>
                </div>
            </div>            
            <div class="modal-footer">
                <a class="waves-effect waves-light btn ddrive modal-close" onclick="enviar('insert')"><i class="material-icons left">save</i>Salvar</a>
                <a class="waves-effect waves-light btn red modal-close"><i class="material-icons left">clear</i>Cancelar</a>
            </div>
        </div>
        
        <div id="modal_loading" class="modal">
             <center>
                <div class="modal-content">
                    <div class="preloader-wrapper big active">
                        <div class="spinner-layer spinner-blue-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                                <div class="circle"></div>
                            </div><div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                 </div>
             </center>
        </div>

    </body>
</html>
<?php 
    session_start();
    include_once 'connect.php';
    if(!isset ($_SESSION['logado']))
    	header('location:index.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Conecty</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link rel = "stylesheet" href = "https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="css/materialize.css">
        <link rel="stylesheet" href="css/style.css">
        <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <link href="https://unpkg.com/tabulator-tables@4.2.4/dist/css/tabulator.min.css" rel="stylesheet">
        <script type="text/javascript" src="https://unpkg.com/tabulator-tables@4.2.4/dist/js/tabulator.min.js"></script>
        <meta charset="utf8">
    </head>
    
    <script type="text/javascript" language="javascript">
        var op;
        var table;

        $(document).ready(function(){
            $('.modal').modal();
            $('select').formSelect();
            $('.sidenav').sidenav({
                edge: 'left', // Choose the horizontal origin
                closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
                draggable: true
            });            
            $('.collapsible').collapsible();
            var printIcon = function(cell, formatterParams, onRendered){ 
                return '<a class="waves-effect waves-light btn green modal-trigger" href="#modal_detail " onclick="changeOp(\'detail\')"><i class="material-icons">visibility</i></a>';
            };
            table = new Tabulator("#tb_drivers", {
                ajaxURL:"motoristascontroller.php",
                ajaxConfig:{
                method:"POST", 
                headers: {
                    "Content-type": 'application/x-www-form-urlencoded'
                    },
                },
                pagination:"local",
                paginationSize:10,
                columns:[
                    {title:"Tipo", width:70, field:"fk_driver_type"},
                    {title:"Nome", field:"first_name"},
                    {title:"Sobrenome", field:"last_name"},
                    {title:"Email", field:"email"},
                    {title:"Ver", formatter:printIcon, width:60, align:"center", cellClick:function(e, cell){
                        var data = cell.getRow().getData();
                        modalEdit(data.id, data.fk_driver_type, data.first_name, data.last_name, data.password, data.certificate_numer, data.email, data.credit, data.car_color, data.car_production_year, data.car_plate, data.car_image, data.lat, data.lng, data.status, data.rating, data.review_count, data.driver_image, data.gender, data.registration_date, data.account_number, data.addres, data.info_changed, data.token, data.cpf, data.cnh, data.city, data.car_board, data.renavam, data.rg_img, data.cnh_img, data.rg, data.birth_date);
                    }}
                ],
                layout:"fitColumns",
            });
        });
        
        function modalEdit(id, fk_driver_type, first_name, last_name, password, certificate_number, email, credit, car_color, car_production_year, car_plate, car_image, lat, lng, status, rating, review_count, driver_image, gender, registration_date, account_number, addres, info_changed, token, cpf, cnh, city, car_board, renavam, rg_img, cnh_img, rg, birth_date){
            switch (op){
                case 'detail':
                    document.getElementById('dtid').innerHTML = id;
                    document.getElementById('dtfk_driver_type').innerHTML = fk_driver_type;
                    document.getElementById('dtfirst_name').innerHTML = first_name;
                    document.getElementById('dtlast_name').innerHTML = last_name;
                    document.getElementById('dtpassword').innerHTML = password;
                    document.getElementById('dtcertificate_number').innerHTML = certificate_number  ;
                    document.getElementById('dtemail').innerHTML = email;
                    document.getElementById('dtcredit').innerHTML = credit;
                    document.getElementById('dtcar_color').innerHTML = car_color;
                    document.getElementById('dtcar_production_year').innerHTML = car_production_year;
                    document.getElementById('dtcar_plate').innerHTML = car_plate;
                    document.getElementById('dtcar_image').source = car_image;
                    document.getElementById('dtcar_image').alt = car_image;
                    document.getElementById('dtcoord').innerHTML = lat + ", " + lng;
                    document.getElementById('dtstatus').innerHTML = status;
                    document.getElementById('dtrating').innerHTML = rating;
                    document.getElementById('dtreview_count').innerHTML = review_count;
                    document.getElementById('dtdriver_image').source = driver_image;
                    document.getElementById('dtdriver_image').alt = driver_image;
                    document.getElementById('dtgender').innerHTML = gender;
                    document.getElementById('dtregistration_date').innerHTML = registration_date;
                    document.getElementById('dtaccount_number').innerHTML = account_number;
                    document.getElementById('dtaddress').innerHTML = addres;
                    document.getElementById('dtinfo_changed').innerHTML = info_changed;
                    document.getElementById('dttoken').innerHTML = token;
                    document.getElementById('dtcpf').innerHTML = cpf;
                    document.getElementById('dtcnh').innerHTML = cnh;
                    document.getElementById('dtcity').innerHTML = city;
                    document.getElementById('dtcar_board').innerHTML = car_board;
                    document.getElementById('dtrenavam').innerHTML = renavam;
                    document.getElementById('dtrg_img').source = rg_img;
                    document.getElementById('dtrg_img').alt = rg_img;
                    document.getElementById('dtcnh_img').source = cnh_img;
                    document.getElementById('dtcnh_img').alt = cnh_img;
                    document.getElementById('dtrg').innerHTML = rg;
                    document.getElementById('dtbirth_date').innerHTML = birth_date;
            }
            M.updateTextFields();
        }
        
        function filtrar(){
            table.clearFilter();
            var campo = document.getElementById('campo').value;
            var operador = document.getElementById('operador').value;
            var filtro = document.getElementById('filtro').value;
            table.setFilter(campo, operador, filtro);
        }
        
        function changeOp(newOp){
            op = newOp;
        }
    </script>
    
    <body>
        <header>
           <?php navBar() ?>
        </header>        
        <div class="container">
            <div class="row">
                <h3><center>Motoristas</center></h3>
            </div>
            <div class="row valign-wrapper">
                <div class="input-field col s6 m3">
                    <select id="campo">
                        <option value="id">Id</option>
                        <option value="fk_driver_type">Tipo Motorista</option>
                        <option value="first_name" selected>Nome</option>
                        <option value="last_name">Sobrenome</option>
                        <option value="password">Senha</option>
                        <option value="certificate_number">Certificado</option>
                        <option value="mobile_number">Tel. Móvel</option>
                        <option value="email">Email</option>
                        <option value="credit">Crédito</option>
                        <option value="fk_car_id">Id Carro</option>
                        <option value="car_color">Cor do Carro</option>
                        <option value="car_production_year">Ano Carro</option>
                        <option value="car_plate">Placa</option>
                        <option value="car_image">Imagem Carro</option>
                        <option value="lat">Latitude</option>
                        <option value="lng">Longitude</option>
                        <option value="status">Status</option>
                        <option value="rating">Avaliação</option>
                        <option value="review_count">Qtd. Análises</option>
                        <option value="driver_image">Imagem Motorista</option>
                        <option value="gender">Gênero</option>
                        <option value="registration_date">Data Registro</option>
                        <option value="account_number">Número conta</option>
                        <option value="address">Endereço</option>
                        <option value="info_changed">Trocou Informação</option>
                        <option value="token">Token</option>
                        <option value="cpf">CPF</option>
                        <option value="cnh">CNH</option>
                        <option value="city">Cidade</option>
                        <option value="car_board">Car_board</option>
                        <option value="renavam">Renavam</option>
                        <option value="rg_img">Imagem RG</option>
                        <option value="cnh_img">Imagem CNH</option>
                        <option value="birth_date">Data de Nascimento</option>
                    </select>
                    <label>Campo</label>
                </div>
                <div class="input-field col s4 m2">
                    <select id="operador">
                        <option value="=" selected>Igual</option>
                        <option value="!=">Diferente</option>
                        <option value="like">Contendo</option>
                        <option value="<">Menor</option>
                        <option value="<=">Menor ou Igual</option>
                        <option value=">">Maior</option>
                        <option value=">=">Maior ou igual</option>
                    </select>
                    <label>Comparação</label>
                </div>
                <div class="input-field col s10 m5">
                    <input id="filtro" type="text" class="validate">
                    <label for="filtro">Valor</label>
                </div>
                <a class="waves-effect waves-light ddrive btn" onclick="filtrar()"><i class="material-icons right">filter_list</i>Filtrar</a>
            </div>
            <div class="row" id="tb_drivers">
            </div>  
        </div>
        
        <div id="modal_detail" class="modal">
            <div class="modal-content">
                <center><h5 style="padding-top:10px">Detalhes do motorista</h5></center>
                <div class="row">
                    <p><b>Id: </b><p id="dtid"></p></p>
                </div>
                <div class="row">
                    <p><b>Tipo de motorista: </b><p id="dtfk_driver_type"></p></p>
                </div>
                <div class="row">
                    <p><b>Nome: </b><p id="dtfirst_name"></p></p>
                </div>
                <div class="row">
                    <p><b>Sobrenome: </b><p id="dtlast_name"></p></p>
                </div>
                <div class="row">
                    <p><b>Senha: </b><p id="dtpassword"></p></p>
                </div>
                <div class="row">
                    <p><b>Certificado: </b><p id="dtcertificate_number"></p></p>
                </div>
                <div class="row">
                    <p><b>Telefone móvel: </b><p id="dtmobile_number"></p></p>
                </div>
                <div class="row">
                    <p><b>Email: </b><p id="dtemail"></p></p>
                </div>
                <div class="row">
                    <p><b>Crédito: </b><p id="dtcredit"></p></p>
                </div>
                <div class="row">
                    <p><b>ID do carro: </b><p id="dtfk_car_id"></p></p>
                </div>
                <div class="row">
                    <p><b>Cor do carro: </b><p id="dtcar_color"></p></p>
                </div>
                <div class="row">
                    <p><b>Ano do carro: </b><p id="dtcar_production_year"></p></p>
                </div>
                <div class="row">
                    <p><b>Placa: </b><p id="dtcar_plate"></p></p>
                </div>
                <div class="row">
                    <p><b>Imagem do carro: </b><img id="dtcar_image" alt="Imagem do carro" class="materialboxed" width="150" src=""></p>
                </div>
                <div class="row">
                    <p><b>Coordenadas: </b><p id="dtcoord"></p></p>
                </div>
                <div class="row">
                    <p><b>Status: </b><p id="dtstatus"></p></p>
                </div>
                <div class="row">
                    <p><b>Avaliação: </b><p id="dtrating"></p></p>
                </div>
                <div class="row">
                    <p><b>Quantidade de análises: </b><p id="dtreview_count"></p></p>
                </div>
                <div class="row">
                    <p><b>Imagem do motorista: </b><img id="dtdriver_image" alt="Imagem do motorista" class="materialboxed" width="150" src=""></p>
                </div>
                <div class="row">
                    <p><b>Sexo: </b><p id="dtgender"></p></p>
                </div>
                <div class="row">
                    <p><b>Data de registro: </b><p id="dtregistration_date"></p></p>
                </div>
                <div class="row">
                    <p><b>Número da conta: </b><p id="dtaccount_number"></p></p>
                </div>
                <div class="row">
                    <p><b>Endereço: </b><p id="dtaddress"></p></p>
                </div>
                <div class="row">
                    <p><b>Informações alteradas: </b><p id="dtinfo_changed"></p></p>
                </div>
                <div class="row">
                    <p><b>Token: </b><p id="dttoken"></p></p>
                </div>
                <div class="row">
                    <p><b>CPF: </b><p id="dtcpf"></p></p>
                </div>
                <div class="row">
                    <p><b>CNH: </b><p id="dtcnh"></p></p>
                </div>
                <div class="row">
                    <p><b>Cidade: </b><p id="dtcity"></p></p>
                </div>
                <div class="row">
                    <p><b>Car_Board: </b><p id="dtcar_board"></p></p>
                </div>
                <div class="row">
                    <p><b>Renavam: </b><p id="dtrenavam"></p></p>
                </div>
                <div class="row">
                    <p><b>Imagem do RG: </b><img id="dtrg_img" alt="Imagem do RG" class="materialboxed" width="150" src=""></p>
                </div>
                <div class="row">
                    <p><b>Imagem da CNH: </b><img id="dtcnh_img" alt="Imagem da CNH" class="materialboxed" width="150" src=""></p>
                </div>
                <div class="row">
                    <p><b>RG: </b><p id="dtrg"></p></p>
                </div>
                <div class="row">
                    <p><b>Data de Nascimento: </b><p id="dtbirth_date"></p></p>
                </div>
            </div>
        </div>
    </body>
</html>     
<?php
    session_start();
    include_once 'connect.php';
    if(!isset ($_SESSION['logado']))
    	header('location:index.php');
    $rows[] = array();
    $sql = "SELECT t.*, CONCAT(d.first_name,' ', d.last_name) AS driver_name, d.mobile_number, e.nome from travel t INNER JOIN driver d ON (t.fk_driver = d.id) INNER JOIN empresas e ON (t.fk_empresa = e.id) where t.status = 'travel finished'";
    $result = mysqli_query($link, $sql);                     
    while($row = mysqli_fetch_assoc($result)){
        $rows[] = $row;
    }
    echo '['.substr(json_encode($rows), 4);
    mysqli_close($link);
?>
<?php
    session_start();
    include_once 'connect.php';
    if(!isset ($_SESSION['logado']))
    	header('location:index.php');
    $rows[] = array();
    $tlempresa = 0.0;
    $tldrivers = 0.0;
    $total = 0.0;
    $sqle = "SELECT * FROM empresas";
    $resulte = mysqli_query($link, $sqle);
    while ($rowe = mysqli_fetch_assoc($resulte)){
        $id = $rowe['id'];
        $empresa = $rowe['nome'];
        $taxa = $rowe['taxa'];
        $ttaxa = $rowe['tipo_taxa'];
        $sql = "SELECT t.cost FROM travel t INNER JOIN empresas e ON (t.fk_empresa = e.id) where t.status = 'travel finished' AND e.id = $id";
        $result = mysqli_query($link, $sql);                     
        while($row = mysqli_fetch_assoc($result)){
            $total = $total + $row['cost']/1.0;
            $custo = $row['cost'];
            $lucroempresa = $ttaxa=="PORCENTAGEM" ? ($custo*$taxa/100) : $taxa;
            $lucrodriver = $custo - $lucroempresa;
            $tlempresa = $tlempresa + $lucroempresa;
            $tldrivers = $tldrivers + $lucrodriver;
        }
        $data['empresa'] = $empresa;
        $data['taxa'] = $taxa;
        $data['ttaxa'] = $ttaxa=="PORCENTAGEM" ? "%" : "R$";
        $data['total'] = 'R$ '.number_format($total, 2, ',', '.');
        $data['total_empresa'] = 'R$ '.number_format($tlempresa, 2, ',', '.');
        $data['total_drivers'] = 'R$ '.number_format($tldrivers, 2, ',', '.');
        $rows[] = $data;
        $tlempresa = 0.0;
        $tldrivers = 0.0;
        $total = 0.0;
    }
    echo '['.substr(json_encode($rows), 4);
    mysqli_close($link);
?>
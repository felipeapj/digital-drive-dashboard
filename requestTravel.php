<?php
    ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);
	include_once('connect.php');
	header("Content-Type: application/json; charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT, GET, POST");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
    $inputJSON = file_get_contents('php://input');
	$postdata = json_decode($inputJSON);
	$id_rider = $postdata->id_rider;
	$origin = urldecode($postdata->origin);
	$destination = urldecode($postdata->destination);
	// $from_lat = $postdata->from_lat;
	// $from_lng = $postdata->from_lng;
	// $to_lat = $postdata->to_lat;
	// $to_lng = $postdata->to_lng;
	$distance_best = $postdata->distance_best;
	$duration_best = $postdata->duration_best;

	$address = str_replace(' ', '+', $origin);
    $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&sensor=false&key=AIzaSyA3RmjC_tc5oMQlvwGGuWUxSFy7fbenpCg';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $geoloc = curl_exec($ch);

    $json = json_decode($geoloc);
    $from_lat =  $json->results[0]->geometry->location->lat;
    $from_lng = $json->results[0]->geometry->location->lng;

    $address = str_replace(' ', '+', $destination);
    $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&sensor=false&key=AIzaSyA3RmjC_tc5oMQlvwGGuWUxSFy7fbenpCg';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $geoloc = curl_exec($ch);

    $json = json_decode($geoloc);
    $to_lat =  $json->results[0]->geometry->location->lat;
    $to_lng = $json->results[0]->geometry->location->lng;



	$sql = 'INSERT INTO travel (fk_rider,status,origin,destination,from_lat,from_lng,to_lat,to_lng,distance_best,duration_best,travel_distance,travel_duration,is_hidden) VALUES('.$id_rider.',1,"'.$origin.'","'.$destination.'",'.$from_lat.','.$from_lng.','.$to_lat.','.$to_lng.','.$distance_best.','.$duration_best.','.$distance_best.','.$duration_best.", 0)";
	$conn->query($sql);
	$id = $conn->lastInsertId();
	$retorno = array(
		'response' => array(
			'idTravel' => $id
		)
	);
	
	$sql = 'SELECT T.*, R.first_name, R.last_name, R.image_address FROM travel AS T
INNER JOIN rider AS R ON T.fk_rider = R.id WHERE T.status = "requested"';
	$travels = $conn->query($sql);
	$travels = $travels->fetchAll(PDO::FETCH_ASSOC);
	
		$fields = array
        (
            'data' => array (
                "travel_add" => json_encode( $travels )
            ),
            'to' => '/topics/drivers_online'
        );
        $API_ACCESS_KEY = 'AIzaSyBG4guucR3z4aRTpp73rxgx6wbBxQz5HJc';
        $headers = array
        (
            'Authorization: key=' . $API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

	echo json_encode($retorno);
?>
<?php
	ini_set('display_errors',1);
	ini_set('display_startup_erros',1);
	error_reporting(E_ALL);
	include_once('connect.php');
	header("Content-Type: application/json; charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT, GET, POST");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	$inputJSON = base64_decode( file_get_contents('php://input') );
	$postdata = json_decode($inputJSON);

	if(isset($postdata->cliente)){

		$nome_cliente = base64_encode( $postdata->cliente->nome );
		$email_cliente = base64_encode( $postdata->cliente->email );
		$telefone_cliente = base64_encode( $postdata->cliente->telefone );
		$cpf_cliente = base64_encode( $postdata->cliente->cpf );


		$sql = 'INSERT INTO client_card (nome,email,numero,CPF) VALUES("'.$nome_cliente.'","'.$email_cliente.'", "'.$telefone_cliente.'", "'.$cpf_cliente.'")';
		$conn->query($sql);


		$titular_cartao = $conn->lastInsertId();

		//---1----------------1-1--1-1-1-1-1--11--1-1-1-1-1-1--11--1-1-1-1-1--1-1---

		$numero_cartao = base64_encode( $postdata->cartao->numero );
		$mes_cartao = base64_encode( $postdata->cartao->mes );
		$ano_cartao = base64_encode( $postdata->cartao->ano );
		$cvv_cartao = base64_encode( $postdata->cartao->cvv );
		$nome_cartao = base64_encode( $postdata->nome );
		$idRider = $postdata->idRider;


		$sql = 'INSERT INTO credit_cards (id_rider,nome, numero,mes,ano,cvv,titular) VALUES('.$idRider.', "'.$nome_cartao.'","'.$numero_cartao.'", "'.$mes_cartao.'", "'.$ano_cartao.'", "'.$cvv_cartao.'", '.$titular_cartao.')';
		$conn->query($sql);
		$retorno = array(
			'response' => array(
				'error' => 'false'
			)
		);

		echo json_encode($retorno);

	}else{

		$numero_cartao = base64_encode( $postdata->cartao->numero );
		$mes_cartao = base64_encode( $postdata->cartao->mes );
		$ano_cartao = base64_encode( $postdata->cartao->ano );
		$cvv_cartao = base64_encode( $postdata->cartao->cvv );
		$nome_cartao = base64_encode( $postdata->nome );
		$idRider = $postdata->idRider;


		$sql = 'INSERT INTO credit_cards (id_rider,nome, numero,mes,ano,cvv,titular) VALUES('.$idRider.', "'.$nome_cartao.'","'.$numero_cartao.'", "'.$mes_cartao.'", "'.$ano_cartao.'", "'.$cvv_cartao.'", 0)';
		$conn->query($sql);
		$retorno = array(
			'response' => array(
				'error' => 'false'
			)
		);

		echo json_encode($retorno);

	}
	

?>
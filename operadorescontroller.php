<?php 
    session_start();
    if(!isset ($_SESSION['logado']))
    	header('location:index.php');
    include_once 'connect.php';
    if (isset($_POST['op'])) {
        switch ($_POST['op']){
            case 'insert':                    
                $first_name = strtoupper($_POST['first_name']);
                $last_name = strtoupper($_POST['last_name']);
                $image_address = $_POST['image_address'];
                $email = $_POST['email'];
                $password = $_POST['password'];
                $type = $_POST['type'];
                $mobile_number = str_replace(") ", "", str_replace("(", "", str_replace("-", "",$_POST['mobile_number'])));
                $phone_number = str_replace(") ", "", str_replace("(", "", str_replace("-", "",$_POST['phone_number'])));
                $address = $_POST['address'];
                $access_base_types = $_POST['access_base_types'];
                $access_stats = $_POST['access_stats'];
                $access_users = $_POST['access_users'];
                $access_drivers = $_POST['access_drivers'];
                $access_travels = $_POST['access_travels'];
                $access_complaints = $_POST['access_complaints'];
                $access_call_requests = $_POST['access_call_requests'];
                $access_payment_requests = $_POST['access_payment_requests'];
                $is_revoked = $_POST['is_revoked'];
                $sql = "INSERT INTO operator (first_name , last_name , image_address , email , password , type , mobile_number , phone_number , address , access_base_types , access_stats , access_users , access_drivers , access_travels , access_complaints , access_call_requests , access_payment_requests , is_revoked) VALUES('$first_name', '$last_name', '$image_address', '$email', '$password', '$type', $mobile_number, $phone_number, '$address', $access_base_types, $access_stats, $access_users, $access_drivers , $access_travels, $access_complaints, $access_call_requests, $access_payment_requests, $is_revoked)";
                if (mysqli_query($link, $sql)){
                    echo "TRUE";
                }
                else
                    echo "FALSE";
                mysqli_close($link);
            break;
            case 'update':
                $id = $_POST['id'];
                $first_name = strtoupper($_POST['first_name']);
                $first_name = strlen($first_name) == 0 ? " " : $first_name;
                $last_name = strtoupper($_POST['last_name']);
                $last_name = strlen($last_name  ) == 0 ? " " : $last_name;
                $image_address = $_POST['image_address'];
                $image_address = strlen($image_address) == 0 ? " " : $image_address;
                $email = $_POST['email'];
                $email = strlen($email) == 0 ? " " : $email;
                $password = $_POST['password'];
                $password = strlen($password) == 0 ? " " : $password;
                $type = $_POST['type'];
                $type = strlen($type) == 0 ? 'NULL' : $type;
                $mobile_number = str_replace(") ", "", str_replace("(", "", str_replace("-", "",$_POST['mobile_number'])));
                $mobile_number = strlen($mobile_number) == 0 ? 'NULL' : $mobile_number;
                $phone_number = str_replace(") ", "", str_replace("(", "", str_replace("-", "",$_POST['phone_number'])));
                $phone_number = strlen($phone_number) == 0 ? 'NULL' : $phone_number;
                $address = $_POST['address'];
                $address = strlen($address) == 0 ? " " : $address;
                $access_base_types = $_POST['access_base_types'];
                $access_stats = $_POST['access_stats'];
                $access_users = $_POST['access_users'];
                $access_drivers = $_POST['access_drivers'];
                $access_travels = $_POST['access_travels'];
                $access_complaints = $_POST['access_complaints'];
                $access_call_requests = $_POST['access_call_requests'];
                $access_payment_requests = $_POST['access_payment_requests'];
                $is_revoked = $_POST['is_revoked'];
                $sql = "UPDATE operator SET first_name = '$first_name', last_name = '$last_name', image_address = '$image_address', email = '$email', password = '$password', type = '$type', mobile_number = $mobile_number, phone_number = $phone_number, address = '$address', access_base_types = $access_base_types, access_stats = $access_stats, access_users = $access_users, access_drivers = $access_drivers, access_travels = $access_travels, access_complaints = $access_complaints, access_call_requests = $access_call_requests, access_payment_requests = $access_payment_requests, is_revoked = $is_revoked WHERE id = $id";
                if (mysqli_query($link, $sql)){
                    echo "TRUE";
                }
                else
                    echo mysqli_error($link);
                mysqli_close($link);
            break;
            case 'delete':
                $id = $_POST['id'];
                $sql = "DELETE FROM operator WHERE id = $id";
                if (mysqli_query($link, $sql)){
                    echo "TRUE";
                }
                else
                    echo "FALSE";
                mysqli_close($link);
            break;
            case 'select':
                $rows[] = array();
                $sql = "SELECT * FROM operator";
                $result = mysqli_query($link, $sql);                     
                while($row = mysqli_fetch_assoc($result)){
                     $rows[] = $row; 
                }
                echo '['.substr(json_encode($rows), 4);
                mysqli_close($link);
            break;
                
        }
    }
else
{
    $rows[] = array();
    $sql = "SELECT * FROM operator";
    $result = mysqli_query($link, $sql);                     
    while($row = mysqli_fetch_assoc($result)){
         $rows[] = $row; 
    }
    echo '['.substr(json_encode($rows), 4);
    mysqli_close($link);
}
?>
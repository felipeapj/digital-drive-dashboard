<?php 
    session_start();
    include_once 'connect.php';
    if(!isset ($_SESSION['logado']))
    	header('location:index.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Conecty</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link rel = "stylesheet" href = "https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="css/materialize.css">
        <link rel="stylesheet" href="css/style.css">
        <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <meta charset="utf8">
    </head>
    
    <script type="text/javascript" language="javascript">
        var mdLoading;
        $(document).ready(function(){
            $('.fixed-action-btn').floatingActionButton();
            $('.modal').modal();
            $('.sidenav').sidenav({
                edge: 'left', // Choose the horizontal origin
                closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
                draggable: true
            });            
            $('.collapsible').collapsible();
            mdLoading = M.Modal.getInstance(document.getElementById('modal_loading'));
        });

        function modalEdit(op, id, nome, sobrenome, email, senha){
            if (op=="delete"){
                document.getElementById('delid').value = id;
                document.getElementById('delnome').value = nome + " " + sobrenome;
                document.getElementById('delemail').value = email;
                document.getElementById('delsenha').value = senha;
            }
            else {
                document.getElementById('edtid').value = id;
                document.getElementById('edtnome').value = nome;
                document.getElementById('edtsobrenome').value = sobrenome;
                document.getElementById('edtemail').value = email;
                document.getElementById('edtsenha').value = senha;
            }
            M.updateTextFields();
        }
        
        function enviar(op){            
            var http = new XMLHttpRequest();
            var url = 'operadorescontroller.php';
            mdLoading.open({opacity:1});
            http.open('POST', url, true);
            http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            switch (op){
                case 'delete':
                    var id = document.getElementById('delid').value;
                    var params = "op=delete&id="+id;                    
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Operador deletado', displayLength : 2000});
                                document.getElementById("tb_operadores").innerHTML = location.reload();
                            }
                            else
                                M.toast({html: 'Erro ao deletar operador', displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
                case 'update':
                    var id = document.getElementById('edtid').value;
                    var nome = document.getElementById('edtnome').value;
                    var sobrenome = document.getElementById('edtsobrenome').value;
                    var email = document.getElementById('edtemail').value;
                    var senha = document.getElementById('edtsenha').value;
                    var params = "op=update&id="+id+"&nome="+nome+"&sobrenome="+sobrenome+"&email="+email+"&senha="+senha;
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Operador alterado', displayLength : 2000});
                                document.getElementById("tb_operadores").innerHTML = location.reload();
                            }
                            else
                                M.toast({html: 'Erro ao alterar operador', displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
                case 'insert':
                    var nome = document.getElementById('addnome').value;
                    var sobrenome = document.getElementById('addsobrenome').value;
                    var email = document.getElementById('addemail').value;
                    var senha = document.getElementById('addsenha').value;
                    var params = "op=insert&nome="+nome+"&sobrenome="+sobrenome+"&email="+email+"&senha="+senha;
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Operador criado', displayLength : 2000});
                                document.getElementById("tb_operadores").innerHTML = location.reload();
                            }
                            else
                                M.toast({html: 'Erro ao inserir operador - '+http.responseText, displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
            }
        }
    </script>
    
    <body>
        <header>
           <?php navBar() ?>
        </header>        
        <div class="container">
            <div class="fixed-action-btn">
                <a class="btn-floating btn-large ddrive waves-effect waves-light modal-trigger" href="#modal_insert">
                    <i class="large material-icons">add</i>
                </a>
            </div>
            <div class="row">
                <h3><center>Mileage</center></h3>
            </div>
            <div class="row" id="tb_operadores" style="overflow-x:auto">
                <table class="striped bordered" style="width:100%">
                    <thead>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>Senha</th>
                        <th>Opções</th>
                    </thead>
                    <tbody>
                        <?php 
                            /*$login = $_SESSION['login'];
                            $senha = $_SESSION['senha'];
                            $sql = "SELECT * FROM operadores where codestabelecimento = (SELECT e.id from estabelecimentos e inner join users u on (e.user=u.id)
                            where u.email='$login' and u.senha = '$senha')";
                            $result = $con->query($sql);                        
                            while($row = $result->fetch_assoc()){
                                echo '<tr>';
                                echo '<td>'.$row['nome'].'</td>';
                                echo '<td>'.$row['sobrenome'].'</td>';
                                echo '<td>'.$row['email'].'</td>';
                                echo '<td>'.$row['senha'].'</td>';
                                echo '
                                <td>
                                <a class="waves-effect waves-light btn connecty primary modal-trigger" href="#modal_edit" onclick="modalEdit(\'update\', '.$row['id'].', \''.$row['nome'].'\', \''.$row['sobrenome'].'\', \''.$row['email'].'\', \''.$row['senha'].'\')"><i class="material-icons">edit</i></a>
                                <a class="waves-effect waves-light btn red modal-trigger" href="#modal_delete" onclick="modalEdit(\'delete\', '.$row['id'].', \''.$row['nome'].'\', \''.$row['sobrenome'].'\', \''.$row['email'].'\', \''.$row['senha'].'\')"><i class="material-icons">delete</i></a>
                                </td>';
                                echo '</tr>';
                            }
                            $con->close();*/
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        
        <div id="modal_delete" class="modal">
            <div class="modal-content">
                <center>
                    <h4 style="padding-top:10px">Confirmar Exclusão</h4>
                    <div class="row margin" style="display:none">
                        <div class="input-field col s4">
                            <input id="delid" type="text">
                            <label for="delid" class="active">ID</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s12">
                            <input id="delnome" type="text">
                            <label for="delnome" class="active">Nome</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s6">
                            <input id="delemail" type="text">
                            <label for="delemail" class="active">Email</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s6">
                            <input id="delsenha" type="text">
                            <label class="active" for="delsenha">Senha</label>
                        </div>
                    </div>
                    <a class="waves-effect waves-light btn green modal-close" onclick="enviar('delete')"><i class="material-icons left">check_circle</i>Sim</a>
                    <a class="waves-effect waves-light btn red modal-close"><i class="material-icons left">clear</i>Não</a>
                </center>
            </div>
        </div>
        
        <div id="modal_edit" class="modal">
            <div class="modal-content">
                <center>
                    <h4 style="padding-top:10px">Editar Operador</h4>
                    <div class="row margin" style="display:none">
                        <div class="input-field col s4">
                            <input id="edtid" type="text">
                            <label for="edtid" class="active">ID</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s12">
                            <input id="edtnome" type="text">
                            <label for="edtnome" class="active">Nome</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s12">
                            <input id="edtsobrenome" type="text">
                            <label for="edtsobrenome" class="active">Sobrenome</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s6">
                            <input id="edtemail" type="text">
                            <label for="edtemail" class="active">Email</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s6">
                            <input id="edtsenha" type="text">
                            <label class="active" for="edtsenha">Senha</label>
                        </div>
                    </div>
                    <a class="waves-effect waves-light btn green modal-close" onclick="enviar('update')"><i class="material-icons left">check_circle</i>Sim</a>
                    <a class="waves-effect waves-light btn red modal-close"><i class="material-icons left">clear</i>Não</a>
                </center>
            </div>
        </div>
        
        <div id="modal_insert" class="modal">
            <div class="modal-content">
                <center>
                    <h4 style="padding-top:10px">Inserir Operador</h4>
                    <div class="row margin">
                        <div class="input-field col s12">
                            <input id="addnome" type="text">
                            <label for="addnome" class="active">Nome</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s12">
                            <input id="addsobrenome" type="text">
                            <label for="addsobrenome" class="active">Sobrenome</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s6">
                            <input id="addemail" type="text">
                            <label for="addemail" class="active">Email</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s6">
                            <input id="addsenha" type="text">
                            <label class="active" for="addsenha">Senha</label>
                        </div>
                    </div>
                    <a class="waves-effect waves-light btn green modal-close" onclick="enviar('insert')"><i class="material-icons left">check_circle</i>Salvar</a>
                    <a class="waves-effect waves-light btn red modal-close"><i class="material-icons left">clear</i>Cancelar</a>
                </center>
            </div>
        </div>
        
        <div id="modal_loading" class="modal">
             <center>
                <div class="modal-content">
                    <div class="preloader-wrapper big active">
                        <div class="spinner-layer spinner-blue-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                                <div class="circle"></div>
                            </div><div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                 </div>
             </center>
        </div>

    </body>
</html>
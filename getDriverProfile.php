<?php
	include_once('connect.php');
	header("Content-Type: application/json; charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT, GET, POST");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	$postdata = json_decode(base64_decode($_GET['data']));
	$id = $postdata->idDriver;
	$sql = 'SELECT D.*, C.color as "carro_cor_texto", M.category as "carro_tipo_texto" FROM driver AS D
INNER JOIN color AS C ON D.car_color = C.id
INNER JOIN categories AS M ON D.fk_car_id = M.id WHERE D.id = '.$id;
	$consulta = $conn->prepare($sql);
    $consulta->execute();

    $result = $consulta->fetchAll(PDO::FETCH_ASSOC);

    $retorno = array(
    	'response' => $result[0]
    );

    echo json_encode($retorno);
?>
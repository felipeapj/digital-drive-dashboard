<?php
	include_once('connect.php');
	header("Content-Type: application/json; charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT, GET, POST");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	$postdata = json_decode(base64_decode($_GET['data']));
	$sql = 'SELECT R.*, O.name AS "rider_orgao", C.name as "rider_patente" FROM rider AS R 
	INNER JOIN military_graduation AS C ON R.cargo_militar = C.id
	INNER JOIN military_organization AS O ON R.org_militar = O.id WHERE R.id = '.$postdata->idRider.'';

	$consulta = $conn->prepare($sql);
    $consulta->execute();

    $result = $consulta->fetchAll(PDO::FETCH_ASSOC);

    $retorno = array(
    	'response' => $result[0]
    );

    echo json_encode($retorno);
?>
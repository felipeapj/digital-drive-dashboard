<?php 
    session_start();
    include_once 'connect.php';
    if(!isset ($_SESSION['logado']))
    	header('location:index.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Digital Drive</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link rel = "stylesheet" href = "https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="css/materialize.css">
        <link rel="stylesheet" href="css/style.css">
        <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <meta charset="utf8">
    </head>
    
    <script type="text/javascript" language="javascript">
        $(document).ready(function(){
            $('.sidenav').sidenav({
                edge: 'left', // Choose the horizontal origin
                closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
                draggable: true
            });            
        });
    </script>
    <body>
        <header>
           <?php navBar() ?>
        </header>        
        <div class="container">
            <div class="row">
                <h3><center>Cancelados</center></h3>
            </div>
            <div class="row" id="tb_reviews" style="overflow-x:auto; height: 500px;">
                <table class="striped bordered" style="table-layout: auto">
                    <thead>
                        <th>Motorista</th>
                        <th>Passageiro</th>
                        <th>Status</th>
                        <th>Origem</th>
                        <th>Destino</th>
                        <th>Coordenadas origem</th>
                        <th>Coordenadas destino</th>
                        <th>Melhor distância</th>
                        <th>Melhor duração</th>
                        <th>Tempo</th>
                        <th>Tipo</th>
                        <th>Custo</th>
                        <th>Nota</th>
                        <th>Tipo de pagamento</th>
                        <th>Tempo de início</th>
                        <th>Log</th>
                        <th>Oculto</th>
                    </thead>
                    <tbody>
                        <?php 
                            $sql = "SELECT * FROM travel WHERE status = 'driver canceled' OR status = 'rider canceled'";
                            $result = mysqli_query($link, $sql);                     
                            while($row = mysqli_fetch_assoc($result)){
                                echo '<tr>';
                                echo '<td>'.$row['fk_driver'].'</td>';
                                echo '<td>'.$row['fk_rider'].'</td>';
                                echo '<td>'.$row['status'].'</td>';
                                echo '<td>'.$row['origin'].'</td>';
                                echo '<td>'.$row['destination'].'</td>';
                                echo '<td>'.$row['from_lat'].', '.$row['from_lng'].'</td>';
                                echo '<td>'.$row['to_lat'].', '.$row['to_lng'].'</td>';
                                echo '<td>'.$row['distance_best'].'</td>';
                                echo '<td>'.$row['duration_best'].'</td>';
                                echo '<td>'.$row['travel_duration'].'</td>';
                                echo '<td>'.$row['travel_distance'].'</td>';
                                echo '<td>'.$row['request_time'].'</td>';
                                echo '<td>'.$row['fk_travel_type'].'</td>';
                                echo '<td>'.$row['cost'].'</td>';
                                echo '<td>'.$row['rating'].'</td>';
                                echo '<td>'.$row['bill_type'].'</td>';
                                echo '<td>'.$row['travel_start'].'</td>';
                                echo '<td>'.$row['log'].'</td>';
                                echo '<td>'.($row['is_hidden']==1 ? 'Sim' : 'Não').'</td>';
                                echo '</tr>';
                            }
                            mysqli_close($link);
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <footer class="ddrive"></footer>
    </body>
</html>
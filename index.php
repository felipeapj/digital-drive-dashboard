<!DOCTYPE html>
<html>
   <head>
      <title>Digital Drive</title>
      <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
      <link rel = "stylesheet" href = "https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="css/materialize.css">
      <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
      <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
      <meta charset="utf8">
   </head>
<script type="text/javascript" language="javascript">
    var mdLoading;
    $(document).ready(function(){
    $('.modal').modal();
    mdLoading = M.Modal.getInstance(document.getElementById("modal_loading"));
  });
 
    function enviar(op){
        var http = new XMLHttpRequest();
        var url = 'userController.php';
        mdLoading.open({opacity:1});
        http.open('POST', url, true);
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        switch (op){
            case 'insert':
            var params = "op=insert&data="+toJSONString(document.getElementById('cadastrarForm'));
            http.onreadystatechange = function() {
                if(http.readyState == 4 && http.status == 200) {
                    mdLoading.close();                            
                    if (http.responseText > 0){
                        M.toast({html: 'Usuário cadastrado, faça o log-in', displayLength : 2000});                               
                    }
                    else
                        M.toast({html: 'Erro ao cadastrar Usuário', displayLength : 2000});
                }
            }                    
            http.send(params);
            break;
            case 'login':
                var params = "op=login&email="+document.getElementById('loginemail').value+"&senha="+document.getElementById('loginpassword').value;
                http.onreadystatechange = function() {
                if(http.readyState == 4 && http.status == 200) {
                    mdLoading.close();                            
                    if (http.responseText == "TRUE"){
                        window.location = '/admin/mapa.php';
                    }
                    else
                        M.toast({html: 'Erro', displayLength : 2000});
                }
            }                    
            http.send(params);
                break;
            }
        }
    
    function toJSONString( form ) {
            var obj = {};
            var elements = form.querySelectorAll( "input" );
            for( var i = 0; i < elements.length; ++i ) {
                var element = elements[i];
                var name = element.name;
                var value = element.value;
                if( name ) {
                    obj[ name ] = value;
                }
            }
            return JSON.stringify( obj );
	   }
    
    function submit(e) {
        if (e.keyCode == 13) {
            enviar('login');
        }
    }
        
    function mascara(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascara()",1)
    }

       
    function execmascara(){
        v_obj.value=v_fun(v_obj.value)
    }
    
    function mtel(v){
        v=v.replace(/\D/g,"");            
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); 
        v=v.replace(/(\d)(\d{4})$/,"$1-$2"); 
        return v;
    }
        
    
</script>
    <body class="container ddrive">
		<div class = "row" style="padding-top:100px"></div>
        <div class="card-panel z-depth-4 center-align">
            <div class="row"><span class="flow-text"><h5>Login</h5></span></div>
			<div class="row margin">
				<div class="input-field col s12">
				<i class="mdi-social-person-outline prefix"></i>
				<input type="text" name="email" id="loginemail" autofocus>
				<label for="loginemail" class="center-align">Email</label>
				</div>
			</div>
			<div class="row margin">
				<div class="input-field col s12">
					<i class="mdi-action-lock-outline prefix"></i>
					<input type="password" name="password" id="loginpassword" class="active" onkeypress="submit(event)">
					<label for="loginpassword" >Password</label>
				</div>
			</div>
               <div class="row margin">
                    <label>
                       <input type="checkbox" class="checkbox-ddrive filled-in" name="salvar" id="salvar" checked="checked"/>
                       <span>Salvar Usuário/Senha</span>
                   </label>
               </div>
			<div class="row margin" id="erro" style="display:none">
                   <div class="center-align" style="padding-left:25px"><span class="flow-text"><h6>Usuário/Senha inválidos!</h6></span></div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<button class="btn modal-trigger waves-effect waves-light col s12 ddrive" data-target="modal_loading" onclick="enviar('login')">Entrar</button>
				</div>
			</div>
               <div class="row">
                   <a href="#cadastrar" class="modal-trigger ddrive-text">Ainda não é registrado? <b>Cadastre-se</b></a>
               </div>
        </div>
        
        <div id="cadastrar" class="modal">
            <div class="modal-content">
                <center>
                    <h4 style="padding-top:10px">Cadastro</h4>                    
                </center>
                <form id="cadastrarForm" method="post">
                    <div class="row margin">
                        <div class="input-field col s12 m6">
                            <input id="nome" name="nome" type="text">
                            <label for="nome" class="active">Nome</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s12 m6">
                            <input id="email" name="email" type="email" class="validate">
                            <label for="email" class="active">Email</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s6 m3">
                            <input id="senha" name="senha" type="password">
                            <label for="senha" class="active">Senha</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s7 m3">
                            <input id="telefone" type="text" name="telefone" onkeyup="mascara( this, mtel );" maxlength="15">
                            <label class="active" for="telefone">Telefone</label>
                        </div>
                    </div>
                    <a class="waves-effect waves-light btn red modal-close"><i class="material-icons left">clear</i>Cancelar</a>
                    <a class="waves-effect waves-light btn ddrive modal-close" onclick="enviar('insert')"><i class="material-icons left">check_circle</i>Salvar</a>
                </form>
            </div>
        </div>
         
        <div id="modal_loading" class="modal">
             <center>
                <div class="modal-content">
                    <div class="preloader-wrapper big active">
                        <div class="spinner-layer spinner-blue-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                                <div class="circle"></div>
                            </div><div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                 </div>
             </center>
        </div>
	</body>
</html>
<?php
	include_once('connect.php');
	header("Content-Type: application/json; charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT, GET, POST");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	$postdata = json_decode(base64_decode($_GET['data']));
	$sql = 'SELECT amount, request_date, status FROM payment_request WHERE fk_driver = '.$postdata->idDriver.'';
	$consulta = $conn->prepare($sql);
    $consulta->execute();

    $result = $consulta->fetchAll(PDO::FETCH_ASSOC);

    $retorno = array(
    	'response' => $result
    );

    echo json_encode($retorno);
?>
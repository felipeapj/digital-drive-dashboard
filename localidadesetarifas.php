<?php 
    session_start();
    include_once 'connect.php';
    if(!isset ($_SESSION['logado']))
    	header('location:index.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Digital Drive</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link rel = "stylesheet" href = "https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="css/materialize.css">
        <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script> 
        <meta charset="UTF-8">
    </head>

    <script type="text/javascript" language="javascript">
        var mdLoading;
        var cidadesAutoEl;
        var bairrosAutoEl;
        var cidades_str = "";
        var bairros_str = "";
        
        $(document).ready(function(){
            $('.modal').modal();
            $('select').formSelect();
            //$('input.autocomplete').autocomplete();
            var instance = M.Autocomplete.init(document.getElementById('cidade'), { onAutocomplete: function(val) {
                document.getElementById('btnAdicionar').style.display = "block";
                listarBairros();
            }});
            var instance = M.Autocomplete.init(document.getElementById('bairro'), { onAutocomplete: function(val) {
                getTarifa();
            }});
            mdLoading = M.Modal.getInstance(document.getElementById("modal_loading"));
            cidadesAutoEl = M.Autocomplete.getInstance(document.getElementById('cidade'));
            bairrosAutoEl = M.Autocomplete.getInstance(document.getElementById('bairro'));
            $('.sidenav').sidenav({
                menuWidth: 200, 
                edge: 'left', 
                closeOnClick: false, 
                draggable: true 
            });  
        });
        
        
        function listarCidades(){
            var uf = document.getElementById('estado').value;
            var url = "loctarController.php";  
            var params = "op=getcidades&uf="+uf;
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "POST", url, false );
            xmlHttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xmlHttp.send( params );            
            if (xmlHttp.responseText.length > 2){   
                cidades_str = "";
                var loadingelm = document.getElementById('loading');
                loadingelm.style.display = "block";
                var cidelm = document.getElementById('cidade');
                cidelm.readOnly = false;
                cidelm.value = "";            
                var cidades = JSON.parse(xmlHttp.responseText);
                cidades_str = '{ ';
                cidades.forEach(cidadesAutoComplete);
                cidades_str = cidades_str     + "}";
                cidadesAutoEl.updateData(JSON.parse(cidades_str));
                loadingelm.style.display = "none";
            }
        }
        
        function cidadesAutoComplete(value, index, arr){
            var cidade = value['desc_cidade'];
            if (index == arr.length-1)
                cidades_str = cidades_str + "\"" + cidade + "\":null ";
            else
                cidades_str = cidades_str + "\"" + cidade + "\":null, ";
        }
        
        function getTarifa(){
            document.getElementById('btnEditar').style.display = "block";
        }
        
        function listarBairros(){
            var cid = document.getElementById('cidade').value;
            var url = "loctarController.php";  
            var params = "op=getbairros&cidade="+cid;
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "POST", url, false );
            xmlHttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xmlHttp.send( params );
            if (xmlHttp.responseText.length > 2){
                bairros_str = "";
                var loadingelm = document.getElementById('loading');
                loadingelm.style.display = "block";
                var bairroelm = document.getElementById('bairro');
                bairroelm.readOnly = false;
                bairroelm.value = "";
                var bairros = JSON.parse(xmlHttp.responseText);
                bairros_str = '{';
                bairros.forEach(bairrosAutoComplete);
                bairros_str = bairros_str + "}";
                bairrosAutoEl.updateData(JSON.parse(bairros_str));
                loadingelm.style.display = "none";
            }
            else
                document.getElementById('bairro').value = "SEM BAIRROS CADASTRADOS";
        }
        
        function bairrosAutoComplete(value, index, arr){
            var tarifa = 0;
            if (value['tarifa']!= null)
                tarifa = (value['tarifa']/1.0).toFixed(2);
            var bairro = value['desc_bairro'] + " - Tarifa: R$: " + tarifa;
            if (index == arr.length-1)
                bairros_str = bairros_str + "\"" + bairro + "\":null ";
            else
                bairros_str = bairros_str + "\"" + bairro + "\":null, ";
        }
        
        function enviar(op){
            var http = new XMLHttpRequest();
            var url = 'loctarController.php';
            mdLoading.open({opacity:1});
            http.open('POST', url, true);
            http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            switch (op){
                case 'edttarifa':
                    var estado = document.getElementById('estado').value;
                    var cidade = document.getElementById('cidade').value;
                    var bairro = document.getElementById('bairro').value.split('-')[0];
                    var tarifa = document.getElementById('tarifa').value;
                    var params = "op=edttarifa&estado="+estado+"&cidade="+cidade+"&bairro="+bairro+"&tarifa="+tarifa;
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();                            
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Tarifa alterada', displayLength : 2000}); 
                                document.location.reload(true);
                            }
                            else
                                M.toast({html: 'Erro ao alterar tarifa', displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
                case 'insert':
                    var estado = document.getElementById('addestado').value;
                    var cidade = document.getElementById('addcidade').value;
                    var bairro = document.getElementById('addbairro').value;
                    var tarifa = document.getElementById('addtarifa').value;
                    var params = "op=insert&estado="+estado+"&cidade="+cidade+"&bairro="+bairro+"&tarifa="+tarifa
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();                            
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Bairro adicionado', displayLength : 2000});
                                document.location.reload(true); 
                            }
                            else
                                M.toast({html: 'Erro ao adicionar bairro', displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
            }
        }
        
        function toJSONString( form ) {
            var obj = {};
            var elements = form.querySelectorAll( "input, select" );
            for( var i = 0; i < elements.length; ++i ) {
                var element = elements[i];
                var name = element.name;
                var value = element.value;
                if( name ) {
                    obj[ name ] = value;
                }
            }
            return JSON.stringify( obj );
	   }
        
        function formatarMoeda(inputid) {
            var elemento = document.getElementById(inputid);
            var valor = elemento.value;
            if (valor == null)
                valor = 0.0;
            valor = valor + '';
            valor = parseInt(valor.replace(/[\D]+/g,''));
            valor = valor + '';
            valor = valor.replace(/([0-9]{2})$/g, ",$1");

            if (valor.length > 6) {
                valor = valor.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");
            }

            elemento.value = valor;
        }
        
        function carregaModal(){
            document.getElementById('addestado').value = document.getElementById('estado').value;
            document.getElementById('addcidade').value = document.getElementById('cidade').value;
            M.updateTextFields();
        }
    </script>
    
    <body>
        <header>
            <?php navBar() ?>
        </header>
	   <div class="container" id="main" >
           <div class="row">
               <h3><center>Localidades e Tarifas</center></h3>
           </div>
           <div class="row-margin" id="loading" style="display:none">               
               <div class="preloader-wrapper active">
                   <div class="spinner-layer spinner-blue-only">
                       <div class="circle-clipper left">
                           <div class="circle"></div>
                       </div>
                       <div class="gap-patch">
                           <div class="circle"></div>
                       </div>
                       <div class="circle-clipper right">
                           <div class="circle"></div>
                       </div>
                   </div>
               </div>
           </div>
           <div class="row margin">
               <div class="input-field col s6 m3">
                   <select id="estado" name="estado" onchange="listarCidades()">
                       <option value="" disabled selected>Estado</option>
                       <option value="AC">Acre</option>
                       <option value="AL">Alagoas</option>
                       <option value="AP">Amapá</option>
                       <option value="AM">Amazonas</option>
                       <option value="BA">Bahia</option>
                       <option value="CE">Ceará</option>
                       <option value="DF">Distrito Federal</option>
                       <option value="ES">Espírito Santo</option>
                       <option value="GO">Goiás</option>
                       <option value="MA">Maranhão</option>
                       <option value="MT">Mato Grosso</option>
                       <option value="MS">Mato Grosso do Sul</option>
                       <option value="MG">Minas Gerais</option>
                       <option value="PA">Pará</option>
                       <option value="PB">Paraíba</option>
                       <option value="PR">Paraná</option>
                       <option value="PE">Pernambuco</option>
                       <option value="PI">Piauí</option>
                       <option value="RJ">Rio de Janeiro</option>
                       <option value="RN">Rio Grande do Norte</option>
                       <option value="RS">Rio Grande do Sul</option>
                       <option value="RO">Rondônia</option>
                       <option value="RR">Roraima</option>
                       <option value="SC">Santa Catarina</option>
                       <option value="SP">São Paulo</option>
                       <option value="SE">Sergipe</option>
                       <option value="TO">Tocantins</option>
                   </select>
               </div>           
           </div>           
           <div class="row margin">
               <div class="input-field col s10 m4">            
                   <input type="text" id="cidade" class="autocomplete" onfocusout="listarBairros()" value="SELECIONE O ESTADO" readonly>
                   <label for="cidade">Cidade</label>
                </div>
           </div>      
           <div class="row margin">
               <div class="input-field col s10 m4">            
                   <input type="text" id="bairro" class="autocomplete" value="SELECIONE A CIDADE" readonly>
                   <label for="bairro">Bairro</label>
                </div>
           </div>
           <div class="row margin" id="btnAdicionar" style="display:none">
               <a class="waves-effect waves-light btn ddrive modal-trigger" onclick="carregaModal()" href="#modal_inserir"><i class="material-icons right">add_circle_outline</i>Adicionar bairro</a>
           </div>              
           <div class="row margin" id="btnEditar" style="display:none">
               <a class="waves-effect waves-light btn ddrive modal-trigger" href="#modal_editar"><i class="material-icons right">edit</i>Editar</a>
           </div>
        </div>
        
        <div id="modal_editar" class="modal modal-fixed-footer">
            <div class="modal-content">
                <center><h4 style="padding-top:10px">Editar Tarifa</h4></center>
                <div class="row margin">
                    <div class="input-field col s10 m5">
                        <input type="text" id="tarifa" name="tarifa" pattern="[0-9]*" onkeyup="formatarMoeda('tarifa')">
                        <label for="tarifa">Tarifa</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">  
                <a class="waves-effect waves-light btn materialize-red modal-close"><i class="material-icons left">reply</i>Cancelar</a>
                <button class="btn waves-effect waves-light modal-close" type="submit" name="action" onclick="enviar('edttarifa')"><i class="material-icons right">save</i>Salvar</button>
            </div>        
        </div>
        
        <div id="modal_inserir" class="modal modal modal-fixed-footer">
            <div class="modal-content">
                <center><h4 style="padding-top:10px">Inserir bairro</h4></center>
                <form id="cadastrar" action="#!" method="post">
                    <div class="row margin">
                        <div class="input-field col s6 m2">
                            <input type="text" id="addestado" name="addestado" readonly>
                            <label for="addestado" class="active">Estado</label>
                        </div>
                    </div>
                    <div class="row margin">                        
                        <div class="input-field col s10 m4">
                            <input type="text" name="addcidade" id="addcidade" readonly>
                            <label for="addcidade" class="active">Cidade</label>
                        </div>
                    </div>
                    <div class="row margin">                        
                        <div class="input-field col s10 m4">
                            <input type="text" name="addbairro" id="addbairro">
                            <label for="addbairro">Bairro</label>
                        </div>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s6 m3">
                            <input type="text" id="addtarifa" name="addtarifa" pattern="[0-9]*" onkeyup="formatarMoeda('addtarifa')">
                            <label for="addtarifa">Tarifa</label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">  
                <a class="waves-effect waves-light btn materialize-red modal-close"><i class="material-icons left">reply</i>Cancelar</a>
                <button class="btn waves-effect waves-light modal-close" type="submit" name="action" onclick="enviar('insert')"><i class="material-icons right">save</i>Salvar</button>
            </div>        
        </div>
                
        <div id="modal_loading" class="modal">
             <center>
                <div class="modal-content">
                    <div class="preloader-wrapper big active">
                        <div class="spinner-layer spinner-blue-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                                <div class="circle"></div>
                            </div><div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                 </div>
             </center>
        </div>
</body>
</html>
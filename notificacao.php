<?php 
    session_start();
    include_once 'connect.php';
    if(!isset ($_SESSION['logado']))
    	header('location:index.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Conecty</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link rel = "stylesheet" href = "https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="css/materialize.css">
        <link rel="stylesheet" href="css/style.css">
        <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <meta charset="utf8">
    </head>
    
    <script type="text/javascript" language="javascript">
        $(document).ready(function(){
            $('.sidenav').sidenav({
                edge: 'left', // Choose the horizontal origin
                closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
                draggable: true
            });            
        });
        
    </script>
    
    <body>
        <header>
           <?php navBar() ?>
        </header>        
        <div class="container">
            <div class="row">
                <h3><center>Notificação</center></h3>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <input id="notificacao" type="text" class="validate" maxlength="200">
                    <label for="notificacao">Notificação</label>
                </div>
            </div>
            <div class="row margin">
                <a class="waves-effect waves-light ddrive btn" onclick="M.toast({html: 'Enviando', displayLength: '2000'})"><i class="material-icons right">send</i>Enviar</a>
            </div>
        </div>

    </body>
</html>
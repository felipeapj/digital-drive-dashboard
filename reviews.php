<?php 
    session_start();
    include_once 'connect.php';
    if(!isset ($_SESSION['logado']))
    	header('location:index.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Digital Drive</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link rel = "stylesheet" href = "https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="css/materialize.css">
        <link rel="stylesheet" href="css/style.css">
        <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <meta charset="utf8">
    </head>
    
    <script type="text/javascript" language="javascript">
        $(document).ready(function(){
            $('.sidenav').sidenav({
                edge: 'left', // Choose the horizontal origin
                closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
                draggable: true
            });            
        });
    </script>
    <body>
        <header>
           <?php navBar() ?>
        </header>        
        <div class="container">
            <div class="row">
                <h3><center>Reviews</center></h3>
            </div>
            <div class="row" id="tb_reviews" style="overflow-x:auto; height: 500px;">
                <table class="striped bordered" style="table-layout: auto">
                    <thead>
                        <th>Motorista</th>
                        <th>Passageiro</th>
                        <th>Status</th>
                        <th>Review</th>
                    </thead>
                    <tbody>
                        <?php 
                            $sql = "SELECT * FROM driver_review";
                            $result = mysqli_query($link, $sql);                     
                            while($row = mysqli_fetch_assoc($result)){
                                echo '<tr>';
                                echo '<td>'.$row['fk_rider'].'</td>';
                                echo '<td>'.$row['fk_driver'].'</td>';
                                echo '<td>'.$row['score'].'</td>';
                                echo '<td>'.$row['review'].'</td>';
                                echo '</tr>';
                            }
                            mysqli_close($link);
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
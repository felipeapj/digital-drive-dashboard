<?php 
    setlocale(LC_ALL, 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Digital Drive</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link rel = "stylesheet" href = "https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="css/materialize.css">
        <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script> 
        <meta charset="UTF-8">
    </head>

    <script type="text/javascript" language="javascript">
        var fab_ids = [];
        var index = 0;
        function listarMarcas(){
            fab_ids = [];
            selectMarcas = document.getElementById('marcas');
            var url = "http://fipeapi.appspot.com/api/1/carros/marcas.json";
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "GET", url, true );
            xmlHttp.onreadystatechange = function() {
                if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                    var marcasj = JSON.parse(xmlHttp.responseText);
                    var marcas = xmlHttp.responseText;     
                    for (var i=0;i<marcasj.length;i++) {
                        var id = marcasj[i].id;
                        var fabricante = marcasj[i].fipe_name;
                        var httpMarca = new XMLHttpRequest();
                        var urlMarca = "carController.php"
                        httpMarca.open('POST', urlMarca, false);
                        httpMarca.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                        var params = "op=insertmarcas&id="+id+"&fabricante="+fabricante;
                        httpMarca.send(params);
                        fab_ids.push(id);
                    }
                    document.getElementById('btnModelos').disabled = false;
                }
            }
            xmlHttp.send( null );
        }
        
        function cadastrarModelos(){
            for (var i=0; i<fab_ids.length; i++){
                setTimeout(function(){
                    getModelos();
                }, 5 * 1000 * i);
            }
        }
        
        function getModelos(){
            var id = fab_ids[index];
            index++;
            var url = "http://fipeapi.appspot.com/api/1/carros/veiculos/"+id+".json";
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "GET", url, false );
            xmlHttp.send( null );
            var modelosj = JSON.parse(xmlHttp.responseText);
            for (var j=0; j < modelosj.length ; j++){
                var mid = modelosj[j].id;
                var modelo = modelosj[j].fipe_name;
                var http = new XMLHttpRequest();
                var url = "carController.php"
                http.open('POST', url, false);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                var params = "op=insertmodelos&id="+mid+"&fabricante_id="+id+"&modelo="+modelo;
                http.send(params);
                console.log(params);
                console.log(http.responseText);
            }
        }
    </script>
    
    <body>
	   <div class="container">
           <div class="row">
               <h3><center>Carros</center></h3>
           </div>  
           <div class="row margin">
               <button class="waves-effect waves-light btn ddrive" onclick="listarMarcas()">Cadastrar marcas</button>
            </div>
           <div class="row margin">
               <button class="waves-effect waves-light btn ddrive" disabled id="btnModelos" onclick="cadastrarModelos()">Cadastrar modelos</button>
            </div>
        </div>
           
</body>
</html>
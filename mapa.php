<?php 
    session_start();
    include_once 'connect.php';
    if(!isset ($_SESSION['logado']))
    	header('location:index.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Digital Drive</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link rel = "stylesheet" href = "https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="css/materialize.css">
        <link rel="stylesheet" href="css/style.css">
        <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <meta charset="utf8">
    </head>
    
    <script type="text/javascript" language="javascript">
        $(document).ready(function(){                       
            $('.sidenav').sidenav({
                edge: 'left', // Choose the horizontal origin
                closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
                draggable: true
            });     
        });

    </script>
    
    <body>
        <header>
           <?php navBar() ?>
        </header>        
        <div id="map"></div>
            <script>
                $(document).ready(function(){
                    getLocation();
                });
                
                function getLocation() {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(initMap);
                    }
                }
            
                var map;
                function initMap(position) {
                    map = new google.maps.Map(document.getElementById('map'), {
                        center: {lat: -23.4981826, lng: -46.849779},
                        zoom: 15
                    });
                }
                
            </script>
            <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAKVE5nBe2lyvprvSIgLCJ0SAFenyrgTfU&callback=initMap" async defer></script>
    </body>
</html>
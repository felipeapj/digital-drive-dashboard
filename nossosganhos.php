<?php 
    session_start();
    include_once 'connect.php';
    if(!isset ($_SESSION['logado']))
    	header('location:index.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Digital Drive</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link rel = "stylesheet" href = "https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="css/materialize.css">
        <link rel="stylesheet" href="css/style.css">
        <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <link href="https://unpkg.com/tabulator-tables@4.2.4/dist/css/tabulator.min.css" rel="stylesheet">
        <script type="text/javascript" src="https://unpkg.com/tabulator-tables@4.2.4/dist/js/tabulator.min.js"></script>
        <meta charset="utf8">
    </head>
    
    <script type="text/javascript" language="javascript">
        var op;
        var table;
        var tableEmpresas;
        $(document).ready(function(){
            $('select').formSelect();
            $('.sidenav').sidenav({
                edge: 'left', // Choose the horizontal origin
                closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
                draggable: true
            });
            table = new Tabulator("#tb_ganhos", {
                ajaxURL:"nossosganhoscontroller.php",
                ajaxConfig:{
                    method:"POST", 
                    headers: {
                        "Content-type": 'application/x-www-form-urlencoded'
                    },
                },
                pagination:"local",
                paginationSize:10,
                columns:[
                    {title:"Motorista", field:"driver_name"},
                    {title:"Tel. Móvel", field:"mobile_number"},
                    {title:"Custo", field:"cost"},
                    {title:"Cupom", field:"fk_cupom"}
                ],
                groupBy: "nome",
                layout:"fitColumns",
            });            
            tableEmpresas = new Tabulator("#tb_empresas", {
                ajaxURL:"lucroempresas.php",
                ajaxConfig:{
                    method:"POST", 
                    headers: {
                        "Content-type": 'application/x-www-form-urlencoded'
                    },
                },
                pagination:"local",
                paginationSize:10,
                columns:[
                    {title:"Empresa", field:"empresa"},
                    {title:"Taxa", field:"taxa"},
                    {title:"Tipo", field:"ttaxa"},
                    {title:"Total", field:"total"},
                    {title:"Ganhos Empresa", field:"total_empresa"},
                    {title:"Ganhos Drivers", field:"total_drivers"}
                ],
                layout:"fitColumns",
            });
        });
        
        function filtrar(){
            table.clearFilter();
            var empresa = document.getElementById('empresa').value;
            var campo = document.getElementById('campo').value;
            var operador = document.getElementById('operador').value;
            var filtro = document.getElementById('filtro').value;
            table.setFilter([
                {field: campo, type: operador, value: filtro},
                {field: 'fk_empresa', type: '=', value: empresa}
            ]);
        }
    </script>
    
    <body>
        <header>
           <?php navBar() ?>
        </header>        
        <div class="container">
            <div class="row">
                <h3><center>Nossos Ganhos</center></h3>
            </div>
            <div class="row">
                <h4>Empresas</h4>
            </div>
            <div class="row" id="tb_empresas">
            
            </div>
            <div class="row" style="padding-top:20px">
                <h4>Corridas</h4>
            </div>
            <div class="row">
                <div class="input-field col s6 m3">
                    <select id="empresa">
                        <?php 
                            $sql = "SELECT * FROM empresas";
                            $result = mysqli_query($link, $sql);                     
                            while($row = mysqli_fetch_assoc($result)){
                                $id = $row['id'];
                                $empresa = $row['nome'];
                                echo "<option value='$id'>$empresa</option>";   
                            }
                            mysqli_close($link);
                        ?>
                    </select>
                    <label>Empresa</label>
                </div>
            </div>
            <div class="row valign-wrapper">
                <div class="input-field col s6 m3">
                    <select id="campo">
                        <option value="id" selected>Id</option>
                        <option value="fk_driver">Motorista</option>
                        <option value="fk_rider">Passageiro</option>
                        <option value="request_time">Data</option>
                    </select>
                    <label>Campo</label>
                </div>
                <div class="input-field col s4 m2">
                    <select id="operador">
                        <option value="=" selected>Igual</option>
                        <option value="!=">Diferente</option>
                        <option value="like">Contendo</option>
                        <option value="<">Menor</option>
                        <option value="<=">Menor ou Igual</option>
                        <option value=">">Maior</option>
                        <option value=">=">Maior ou igual</option>
                    </select>
                    <label>Comparação</label>
                </div>
                <div class="input-field col s10 m5">
                    <input id="filtro" type="text" class="validate">
                    <label for="filtro">Valor</label>
                </div>
                <a class="waves-effect waves-light ddrive btn" onclick="filtrar()"><i class="material-icons right">filter_list</i>Filtrar</a>
            </div>
            <div class="row" id="tb_ganhos">
            </div>
        </div>
    </body>
</html>
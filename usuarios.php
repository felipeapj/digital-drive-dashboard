<?php 
    session_start();
    include_once 'connect.php';
    if(!isset ($_SESSION['logado']))
    	header('location:index.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Digital Drive</title>
        <meta name = "viewport" content = "width = device-width, initial-scale = 1">      
        <link rel = "stylesheet" href = "https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="css/materialize.css">
        <link rel="stylesheet" href="css/style.css">
        <script type = "text/javascript" src = "https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-rc.2/js/materialize.min.js"></script>
        <meta charset="utf8">
    </head>
    
    <script type="text/javascript" language="javascript">
        var mdLoading;
        $(document).ready(function(){
            $('.fixed-action-btn').floatingActionButton();
            $('.modal').modal();
            $('.sidenav').sidenav({
                edge: 'left', // Choose the horizontal origin
                closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
                draggable: true
            });            
            $('.collapsible').collapsible();
            mdLoading = M.Modal.getInstance(document.getElementById('modal_loading'));
        });

        function modalEdit(op, id, nome, email, senha, telefone){
            if (op=="delete"){
                document.getElementById('delid').value = id;
                document.getElementById('delnome').value = nome;
                document.getElementById('delemail').value = email;
            }
            else {
                document.getElementById('edtid').value = id;
                document.getElementById('edtnome').value = nome;
                document.getElementById('edtemail').value = email;
                document.getElementById('edtsenha').value = senha;
                document.getElementById('edttelefone').value = telefone;
            }
            M.updateTextFields();
        }
        
        function mascara(o,f){
            v_obj=o
            v_fun=f
            setTimeout("execmascara()",1)
        }
    
           
        function execmascara(){
            v_obj.value=v_fun(v_obj.value)
        }
        
        function mtel(v){
            v=v.replace(/\D/g,"");            
            v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); 
            v=v.replace(/(\d)(\d{4})$/,"$1-$2"); 
            return v;
        }        
        
        function enviar(op){            
            var http = new XMLHttpRequest();
            var url = 'usuarioscontroller.php';
            mdLoading.open({opacity:1});
            http.open('POST', url, true);
            http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            switch (op){
                case 'delete':
                    var id = document.getElementById('delid').value;
                    var params = "op=delete&id="+id;                    
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Usuario deletado', displayLength : 2000});
                                document.getElementById("tb_usuarios").innerHTML = location.reload();
                            }
                            else
                                M.toast({html: 'Erro ao deletar usuario', displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
                case 'update':
                    var id = document.getElementById('edtid').value;
                    var nome = document.getElementById('edtnome').value;
                    var email = document.getElementById('edtemail').value;
                    var senha = document.getElementById('edtsenha').value;
                    var telefone = document.getElementById('edttelefone').value;
                    var params = "op=update&id="+id+"&nome="+nome+"&email="+email+"&senha="+senha+"&telefone="+telefone;
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'Usuario alterado', displayLength : 2000});
                                document.getElementById("tb_usuarios").innerHTML = location.reload();
                            }
                            else
                                M.toast({html: 'Erro ao alterar usuario', displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
                case 'insert':
                    var nome = document.getElementById('addnome').value;
                    var email = document.getElementById('addemail').value;
                    var senha = document.getElementById('addsenha').value;
                    var telefone = document.getElementById('addtelefone').value;
                    var params = "op=insert&nome="+nome+"&email="+email+"&senha="+senha+"&telefone="+telefone;
                    http.onreadystatechange = function() {
                        if(http.readyState == 4 && http.status == 200) {
                            mdLoading.close();
                            if (http.responseText=='TRUE'){
                                M.toast({html: 'USuario criado', displayLength : 2000});
                                document.getElementById("tb_usuarios").innerHTML = location.reload();
                            }
                            else
                                M.toast({html: 'Erro ao inserir usuario', displayLength : 2000});
                        }
                    }                    
                    http.send(params);
                    break;
            }
        }
    </script>
    
    <body>
        <header>
           <?php navBar() ?>
        </header>        
        <div class="container">
            <div class="fixed-action-btn">
                <a class="btn-floating btn-large ddrive waves-effect waves-light modal-trigger" href="#modal_insert">
                    <i class="large material-icons">add</i>
                </a>
            </div>
            <div class="row">
                <h3><center>Usuários</center></h3>
            </div>
            <div class="row" id="tb_usuarios" style="overflow-x:auto">
                <table class="striped bordered" style="width:100%">
                    <thead>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>Senha</th>
                        <th>Telefone</th>
                        <th>Opções</th>
                    </thead>
                    <tbody>
                        <?php 
                            $sql = "SELECT * FROM users";
                            $result = mysqli_query($link, $sql);                     
                            while($row = mysqli_fetch_assoc($result)){
                                echo '<tr>';
                                echo '<td>'.$row['nome'].'</td>';
                                echo '<td>'.$row['email'].'</td>';
                                echo '<td>'.$row['senha'].'</td>';
                                echo '<td>'.$row['telefone'].'</td>';
                                echo '
                                <td>
                                <a class="waves-effect waves-light btn ddrive modal-trigger" href="#modal_edit" onclick="modalEdit(\'update\', '.$row['id'].', \''.$row['nome'].'\', \''.$row['email'].'\', \''.$row['senha'].'\', \''.$row['telefone'].'\')"><i class="material-icons">edit</i></a>
                                <a class="waves-effect waves-light btn red modal-trigger" href="#modal_delete" onclick="modalEdit(\'delete\', '.$row['id'].', \''.$row['nome'].'\', \''.$row['email'].'\', \''.$row['senha'].'\', \''.$row['telefone'].'\')"><i class="material-icons">delete</i></a>
                                </td>';
                                echo '</tr>';
                            }
                            mysqli_close($link);
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        
        <div id="modal_delete" class="modal">
            <div class="modal-content">
                <center>
                    <h4 style="padding-top:10px">Confirmar Exclusão</h4>
                </center>
                <div class="row margin" style="display:none">
                    <div class="input-field col s4">
                        <input id="delid" type="text">
                        <label for="delid" class="active">ID</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="delnome" type="text">
                        <label for="delnome" class="active">Nome</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="delemail" type="text">
                        <label for="delemail" class="active">Email</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a class="waves-effect waves-light btn ddrive modal-close" onclick="enviar('delete')"><i class="material-icons left">check_circle</i>Sim</a>
                <a class="waves-effect waves-light btn red modal-close"><i class="material-icons left">clear</i>Cancelar</a>
            </div>
        </div>
        
        <div id="modal_edit" class="modal">
            <div class="modal-content">
                <center>
                    <h4 style="padding-top:10px">Editar Usuário</h4>
                </center>
                <div class="row margin" style="display:none">
                    <div class="input-field col s4">
                        <input id="edtid" type="text">
                        <label for="edtid" class="active">ID</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="edtnome" type="text">
                        <label for="edtnome" class="active">Nome</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="edtemail" type="text">
                        <label for="edtemail" class="active">Email</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="edtsenha" type="text">
                        <label class="active" for="edtsenha">Senha</label>
                    </div>
                </div> 
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="edttelefone" type="text" onkeyup="mascara( this, mtel );" maxlength="15">
                        <label for="edttelefone" class="active">Telefone</label>
                    </div>
                </div>   
            </div>                            
            <div class="modal-footer">
                <a class="waves-effect waves-light btn ddrive modal-close" onclick="enviar('update')"><i class="material-icons left">save</i>Alterar</a>
                <a class="waves-effect waves-light btn red modal-close"><i class="material-icons left">clear</i>Cancelar</a>
            </div>
        </div>
        
        <div id="modal_insert" class="modal">
            <div class="modal-content">
                <center>
                    <h4 style="padding-top:10px">Inserir Operador</h4>
                </center>
                <div class="row margin">
                    <div class="input-field col s12">
                        <input id="addnome" type="text">
                        <label for="addnome" class="active">Nome</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="addemail" type="text">
                        <label for="addemail" class="active">Email</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s6">
                        <input id="addsenha" type="text">
                        <label class="active" for="addsenha">Senha</label>
                    </div>
                    <div class="row margin">
                        <div class="input-field col s12">
                            <input id="addtelefone" type="text" onkeyup="mascara( this, mtel );" maxlength="15">
                            <label for="addtelefone" class="active">Telefone</label>
                        </div>
                    </div>   
                </div>     
            </div>              
            <div class="modal-footer">
                <a class="waves-effect waves-light btn ddrive modal-close" onclick="enviar('insert')"><i class="material-icons left">save</i>Salvar</a>
                <a class="waves-effect waves-light btn red modal-close"><i class="material-icons left">clear</i>Cancelar</a>
            </div>
        </div>
        
        <div id="modal_loading" class="modal">
             <center>
                <div class="modal-content">
                    <div class="preloader-wrapper big active">
                        <div class="spinner-layer spinner-blue-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                                <div class="circle"></div>
                            </div><div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                 </div>
             </center>
        </div>

    </body>
</html>
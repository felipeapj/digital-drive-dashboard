<?php
	include_once('connect.php');
	header("Content-Type: application/json; charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: PUT, GET, POST");
	header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	$postdata = json_decode(base64_decode($_GET['data']));
    $travel = $postdata->idTravel;
	$sql = 'UPDATE travel SET status = "travel started" WHERE id = '.$travel.'';
	$conn->query($sql);

	$sql = "SELECT T.*, R.token FROM travel as T
	INNER JOIN rider AS R ON T.fk_rider = R.id 
	WHERE T.id = ".$travel;
	$consulta = $conn->prepare($sql);
	$consulta->execute();
	$travel_json = $consulta->fetchAll(PDO::FETCH_ASSOC);
	$travel_json = $travel_json[0];
	$token = $travel_json['token'];
	unset($travel_json['token']);

	$fields = array
        (
            'data' => array (
                "travel" => $travel_json
            ),
            'to' => $token
        );
        $API_ACCESS_KEY = 'AIzaSyBG4guucR3z4aRTpp73rxgx6wbBxQz5HJc';
        $headers = array
        (
            'Authorization: key=' . $API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

    echo '{}';
?>